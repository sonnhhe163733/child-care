
<%@page import="model.User" %>
<%@page import="model.User" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Reset Password</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">   
        <style>
            body {
                background-image: url("img/login_bgr.png");
                font-family: Arial, sans-serif;
                background-size: cover;
            }

            .container {
                max-width: 600px;
                margin: 70px auto;
                padding: 40px 40px;
                background-color:#ffffff;
                border-bottom: solid #666666 2px;
                border-right: solid #666666 2px;
                border-radius: 30px;
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
            }

            .form-group {
                margin-bottom: 20px;
            }

            .form-group label {
                display: block;
                margin-bottom: 5px;
            }

            .form-group input[type="text"], input[type="password"]{
                width: 100%;
                padding: 10px 30px;
                border-radius: 10px;
                outline: none;
                border: 1px solid #848e9f;
            }

            .form-group input[type="text"]:focus{
                color: #666666;

            }

            .form-group input[type="submit"], input[type="button"] {
                padding: 10px;
                font-weight: bold;
                border-radius: 10px;
                background-color: #13c5dd;
                color: #ffffff;
                border: none;
                cursor: pointer;
                width: 130px
            }

            .form-group button {
                padding: 10px;
                border-radius: 10px;
                background-color: #cccccc;
                border: none;
                cursor: pointer;

            }

            .form-group a {
                font-weight: bold;
                text-decoration: none;
                color: #333333
            }
            .icon{
                border:solid #13c5dd 2px;
                border-radius:10px;
                background-color: #13c5dd;

            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </head>
    <body>
        <div ${step==1||step==null?"":"hidden"} class="container">
            <h2 class="text-center" style="color: #13c5dd;">Reset Password</h2>
            <form action="password_reset" method="post">
                <input hidden name="step" value="1">
                <div class="form-group">
                    <label for="email">Enter your phone number or email to continue.</label>
                    <input class="mr-5 bg-transparent" type="text" name="identifier" required>
                    <div></div>
                </div>
                <div class="form-group" style="text-align: right">
                    <button><a href="login">Cancel</a></button>
                    <input type="submit" value="Find">
                </div>
            </form>
        </div>
        <div ${step==2?"":"hidden"} class="container" onchange="">
            <h2 class="text-center" style="color: #13c5dd;">Account found!</h2>
            <form id="form_2" action="password_reset" method="post">
                <input hidden name="step" value="2">
                <div class="form-group">
                    <label for="otp"> Validation code was sent to your email "${sessionScope.email}".<br> Check your mail and enter OTP:</label>
                    <i><div id="timer"></div></i>
                    <input class="mr-5 bg-transparent" type="text" name="otp" id="otp" maxlength="6" required>
                    <div id="message" hidden style="color: red">OTP is incorrect.</div>

                </div>
                <div class="form-group" style="text-align: right">
                    <button><a href="login">Cancel</a></button>
                    <input id="button1" type="button" value="Submit" onclick="validateOTP(otp)">
                    <input id="button2" type="button" onClick="window.location.reload();" value="Resend OTP" hidden>
                </div>
            </form>
        </div>
        <div ${step==3?"":"hidden"} class="container">
            <h2 class="text-center" style="color: #13c5dd;">Change Password</h2>
            <form action="password_reset" method="post" onsubmit="alert('Your password has been reset!')">
                <input hidden name="step" value="3">
                <div class="form-group">
                    <label for="newPass">New Password:</label>
                    <input class="mr-5 bg-transparent" type="password" 
                           oninput="checkPass(newPass, rePass)" value="${txt}" 
                           id="newPass" name="newPass" required>
                    <div id="newMess" hidden style="color: red">Password is not valid.
                        Password must contain at least 1 uppercase character, 
                        1 digit and 1 special letter.</div>
                </div>
                <div class="form-group">
                    <label for="rePass">Confirm Password:</label>
                    <input class="mr-5 bg-transparent" type="password" 
                           oninput="checkPass(newPass, rePass)" value="${txt}" 
                           id="rePass" name="rePass" required>
                    <div id="reMess" hidden style="color: red">Re-password must 
                        be similar to new password.</div>
                </div>
                <div class="form-group">
                    <input id="button-change" type="submit" value="Change" disabled>
                </div>
            </form>
        </div>
        <script>
            function timer(remaining) {
                //                var m = Math.floor(remaining / 60);
                var s = remaining;
                //                m = m < 10 ? '0' + m : m;
                s = s < 10 ? '0' + s : s;
                document.getElementById('timer').innerHTML = "Your OTP will exprie in " + s + "s";
                remaining -= 1;
                if (remaining >= 0) {
                    setTimeout(function () {
                        timer(remaining);
                    }, 1000);
                    return;
                }
                document.getElementById('timer').innerHTML = "Your OTP has expried. Click \"Resend OTP\"";
                document.getElementById('otp').disabled = true;
                document.getElementById('button1').hidden = true;
                document.getElementById('button2').hidden = false;
                session.removeAttribute("otp");
            }
            timer(60);

            function validateOTP(param) {
                var otp = param.value;
                $.ajax({
                    url: "password_reset",
                    type: "get",
                    data: {
                        otp: otp,
                        step: "2"
                    },
                    success: function (response) {
                        if (response === "fail") {
                            var row = document.getElementById("message");
                            row.hidden = false;
                        } else {
                            $("#form_2").trigger("submit");
                        }
                    }
                });
            }
            ;

            function checkPass(newPass, rePass) {
                var newPass = newPass.value;
                var rePass = rePass.value;
                $.ajax({
                    url: "password_change_ajax",
                    type: "get",
                    data: {
                        newTxt: newPass,
                        reTxt: rePass
                    },
                    success: function (data) {
                        var newMess = document.getElementById("newMess");
                        var reMess = document.getElementById("reMess");
                        newMess.hidden = true;
                        reMess.hidden = true;
                        const mess = data.split(" ");
                        var m=0;
                        for (var i = 0; i < mess.length; i++) {
                            if (mess[i] === "newMess") {
                                newMess.hidden = false;
                                m+=1;
                            }
                            if (mess[i] === "reMess") {
                                reMess.hidden = false;
                                m+=1;
                            }
                        }
                        if(m===0)
                            document.getElementById("button-change").disabled=false;
                    }
                });
            }

        </script>
    </body>
</html>
