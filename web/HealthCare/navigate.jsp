<%-- 
    Document   : navigate
    Created on : Jun 1, 2023, 3:40:06 PM
    Author     : Son Nguyen
--%>
<%@page import="model.User" %>


<!DOCTYPE html>
<html lang="en">
    
    


    <body>
        <div class="container-fluid py-2 border-bottom d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-center text-lg-start mb-2 mb-lg-0">
                        <div class="d-inline-flex align-items-center">
                            <a class="text-decoration-none text-body pe-3" href=""><i class="bi bi-telephone me-2"></i>+84982 469 362</a>
                            <span class="text-body">|</span>
                            <a class="text-decoration-none text-body px-3" href=""><i class="bi bi-envelope me-2"></i>childrencare68@gmail.com</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <div class="container-fluid sticky-top bg-white shadow-sm">
        <div class="container">
            <nav class="navbar navbar-expand-lg bg-white navbar-light py-3 py-lg-0">
                <a href="home" class="navbar-brand">
                    <h1 class="m-0 text-uppercase text-primary"><i class="fa fa-clinic-medical me-2"></i>Medinova</h1>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="/SWP391/HealthCare/home" class="nav-item nav-link">Home</a>
                        <a href="/SWP391/HealthCare/service" class="nav-item nav-link">Service</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                            <div class="dropdown-menu m-0">
                                <a href="/SWP391/HealthCare/postlist" class="dropdown-item">Blog Grid</a>
                                <a href="/SWP391/HealthCare/doctor_profile" class="dropdown-item">The Team</a>
                                <a href="/SWP391/HealthCare/question" class="dropdown-item">Question</a>
                              
                            </div>          
                        </div>
                        <%if(user==null){%>
                        <a href="/SWP391/login" class="nav-item nav-link">Login</a>
                        <a href="/SWP391/register" class="nav-item nav-link">Register</a>

                        <%}else{%>
                        <a href="/SWP391/logout" class="nav-item nav-link">Logout</a> 
                        <a href="/SWP391/profile_detail" >
                            <img style="width:60px;height:60px;border-radius:45px;border:solid #cccccc 2px;margin:10px 0px;margin-left:35px" src="<%=user.getAvatarUrl()%>" alt="Profile"> 
                        </a>
                        <%}%>
                    </div>
                </div>
            </nav>
        </div>
    </div>
  
</body>
</html>
