
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Category" %>
<%@page import="model.User" %>
<%@page import="dao.post.PostListDAO" %>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>MEDINOVA - Hospital Website</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">  

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

        <!-- Customized Bootstrap Stylesheet -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">


    </head>
    <body>      
        <%User user=(User)session.getAttribute("user");%>
        <%@include file="popup.jsp" %>
        <%@include file="navigate.jsp" %>
        <!-- Hero Start -->
        <div class="container-fluid bg-primary py-5 mb-5 hero-header">
            <div class="container py-5">
                <div class="row justify-content-start">
                    <div class="col-lg-8 text-center text-lg-start">
                        <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5" style="border-color: rgba(256, 256, 256, .3) !important;">Welcome To Medinova</h5>
                        <h1 class="display-1 text-white mb-md-4">Best Healthcare Solution In Your City</h1>
                        <h3 style="color:white">SEARCH YOUR DOCTOR</h3>
                        <div class="pt-2">
                            <form action="search" method="post" >
                                <input name="index" value="1" hidden>
                                <div class="mx-auto" style="width: 100%; padding-right: 100px;">
                                    <div class="input-group">
                                        <select style="width: 20% !important; height: 60px;" class="form-select border-primary w-25" name="option">
                                            <option selected value="0">Filter By</option>
                                            <option value="1">A-Z</option>
                                            <option value="2">Z-A</option>
                                        </select>
                                        <input style="width: 40% !important;" value="${txtSearch}" name="txtSearch" type="text" class="form-control border-primary w-50" placeholder="Keyword">
                                        <button style="width: 20% !important;" class="btn btn-dark border-0 w-25">Search</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->


        <!-- Services Start -->
        <div class="container-fluid py-5">
            <div class="container">
                <div class="text-center mx-auto mb-5 " style="max-width: 500px;">
                    <h1 class="col-md-10 " style="display: inline;">Excellent Medical Services</h1>
                    <span class="col-md-2">
                        <button class="btn btn-primary" value="" style="background: #f0f0f0;margin-bottom:15px;border:none;border-radius:35px;width:60px;height:38px" onclick="getServiceByCategoryID(this)">
                            <h5 style="color:#13c5dd;border-bottom:solid 2px #13c5dd;" class="text-right">All</h5>
                        </button>
                    </span>
                </div>


                <div class="owl-carousel  team-carousel position-relative text-center mx-auto mb-5 col-md-12" style="max-width: 2500px;">
                    <c:forEach items="${listcc}" var="c">
                        <div class="team-item"  >

                            <button value="${c.category_id}" style="margin-bottom:60px;border:none;border-radius:35px;padding:2px;width:300px;" type="button" onclick="getServiceByCategoryID(this)">   
                                <h5 class="d-inline-block text-primary text-uppercase  " style="border-bottom:#13c5dd solid 2px">${c.category_name}</h5>
                            </button>

                        </div>
                    </c:forEach>
                </div>

                <div class="row g-5" id="services"> 
                    <c:forEach items="${services}" var="s">
                        <div class="service col-xl-3 col-lg-6">
                            <div class="bg-light rounded overflow-hidden" style="height: 400px">
                                <img class="img-fluid w-100" src="${s.image}" style="width: 400px;height:224px;object-fit:cover">

                                <div class="p-4" style="max-height:120px;">

                                    <a class="h3 d-block mb-3" href="service_detail?service_id=${s.id }">${s.name}</a>
                                    <div class="col-md-12 row">
                                        <div class="col-md-9" >
                                            <p style="color: #1d2a4d;text-align:left;font-weight: 600">
                                                Service Price:
                                                <span style="color: #ff6666; font-size: 20px; font-weight: 600;">${s.price} $</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>                                  
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <div class=" col-md-12 text-center mt-3" id="view-more">  
                    <button class="btn btn-primary"  style="background: #f0f0f0;margin-bottom:15px;border:none;border-radius:35px;width:120px;height:38px" onclick="loadMoreService()">
                        <h5 style="color:#13c5dd;border-bottom:solid 2px #13c5dd" class="text-right">View more</h5>
                    </button>
                </div>


                <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

                <script>

                        function loadMoreService() {
                            var amount = document.getElementsByClassName("service").length;
                            var divViewMore = document.getElementById("view-more");

                            console.log(amount);
                            // Gửi yêu cầu AJAX
                            $.ajax({
                                url: "../load_more_service",
                                method: "POST",
                                data: {amount: amount},
                                success: function (response) {
                                    if (response === "<p style=\"color: #ff6666; font-size: 20px; font-weight: 600; text-align: center;\">No more service</p>") {
                                        divViewMore.style.display = "none";
                                    }
                                    var row = document.getElementById("services");
                                    row.innerHTML += response;


                                },
                                error: function (xhr, status, error) {
                                    // Xử lý lỗi (nếu có)
                                    console.log(error);
                                }
                            });
                        }

                        function getServiceByCategoryID(idBtn) {
                            var categoryID = idBtn.value;
                            var divViewMore = document.getElementById("view-more");

                            console.log(categoryID);

                            divViewMore.style.display = "none";
                            // Gửi yêu cầu AJAX
                            $.ajax({
                                url: "../get_service",
                                method: "POST",
                                data: {category_id: categoryID},
                                success: function (response) {
                                    var row = document.getElementById("services");
                                    row.innerHTML = response;
                                },
                                error: function (xhr, status, error) {
                                    // Xử lý lỗi (nếu có)
                                    console.log(error);
                                }
                            });
                        }

                </script>






            </div>
        </div>



        <!-- Team Start -->
        <form>
            <div class="container-fluid py-5">
                <div class="container">
                    <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                        <h1 style="display:">Qualified Healthcare Professionals</h1>
                    </div>
                    <div class="owl-carousel team-carousel position-relative">
                        <c:forEach items="${listdt}" var="o">
                            <div class="team-item">
                                <div class="row g-0 bg-light rounded overflow-hidden">
                                    <div class="col-12 col-sm-12 h-50">
                                        <img src="${o.avatarUrl}" style="object-fit: cover; height:100%">
                                    </div>
                                    <div class="col-12 col-sm-12 h-50 d-flex flex-column">
                                        <div class="mt-auto p-4">
                                            <a href="doctor_profile?uid=${o.id}" >${o.name}</a>
                                            <p class="m-0">Contact</p>
                                            <p class="m-0">${o.email}</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </form>
        <!-- Team End -->


        <!-- Blog Start -->
        <div class="container-fluid py-5">
            <div class="container">
                <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                    <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5">Blog Post</h5>
                    <h1 class="display-4">Our Latest Medical Blog Posts</h1>
                </div>
                <div class="row g-5">
                    <c:forEach items="${listpl}" var="o">
                        <div class="col-xl-3 col-lg-6">
                            <div class="bg-light rounded overflow-hidden">
                                <img class="img-fluid w-100" src="${o.post_souce_url}" alt="">
                                <div class="p-4">
                                    <a class="h3 d-block mb-3" href="postdetail?pid=${o.post_id  }">${o.post_content}</a>
                                    <p class="m-0">${o.post_description}</p>
                                </div>
                                <div class="d-flex justify-content-between border-top p-4">
                                    <div class="d-flex align-items-center">
                                        <img class="rounded-circle me-2" src="img/user.jpg" width="25" height="25" alt="">
                                        <small>${o.post_created_date}</small>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <small class="ms-3"><i class="far fa-eye text-primary me-1"></i>12345</small>
                                        <small class="ms-3"><i class="far fa-comment text-primary me-1"></i>123</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>


                </div>
            </div>
        </div>
        <!-- Blog End -->

        <!--Footer Start 
        <%@include file="footer.jsp" %>
         Footer End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="../lib/easing/easing.min.js"></script>
        <script src="../lib/waypoints/waypoints.min.js"></script>
        <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="../lib/tempusdominus/js/moment.min.js"></script>
        <script src="../lib/tempusdominus/js/moment-timezone.min.js"></script>
        <script src="../lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>

    </body>

</html>