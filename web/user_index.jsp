<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Category" %>

<%@page import="dao.user.ProfileDAO" %>
<%@page import="model.Invoice" %>
<%@page import="java.util.List" %>

<%@page import="model.User" %>

<%@page import="model.Service" %>
<%@page import="model.Medicine" %>
<%@page import="model.MedicineItem" %>
<%@page import="model.MedicalReport" %>

<%@page import="model.Reservation" %>




<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>MEDINOVA - Hospital Website Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free HTML Templates" name="keywords">
        <meta content="Free HTML Templates" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">

        <!-- Icon Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">


    </head>
    <style>
        .item_name{
            padding:10px 20px;
            border-radius: 10px;
            color: #1d2a4d;
            font-weight:600;
            background:#f3f3f3;

        }

        .item_name:hover,img:hover {
            box-shadow: 10px 10px 5px #eceaea;

        }
    </style>

    <body >
        <%User user=(User)session.getAttribute("user");%>
        <%String userRank=(String)session.getAttribute("user_rank");%>
        <%Integer invoiceNumber=(Integer)session.getAttribute("invoice_number");%>
        <%String totalMoney=(String)session.getAttribute("total_money");%>
        <%List<Invoice> invoices=(List<Invoice>)request.getAttribute("invoices");%>
        <% Invoice invoice = (Invoice) request.getAttribute("invoice"); %>  
        <%List<MedicalReport> reports=(List <MedicalReport> )request.getAttribute("reports");%>
        <%List<Reservation> reservations=(List <Reservation> )request.getAttribute("reservations");%>
        <%@include file="HealthCare/popup.jsp" %>
        <%@include file="HealthCare/navigate.jsp" %>
        <div class="main-panel col-md-12 row container-fluid  p-0 ">
            <div class="content col-md-4  pt-5 p-2  ">
                <div class="card card-user " style="box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3);">
                    <div class="card-body">
                        <div class="author col-md-12 row m-3">
                            <div class="col-md-5 p-0 ">
                                <img style="width:150px;height:150px;border:#cccccc solid 2px;border-radius:100px;" class="icon border-gray" src="<%=user.getAvatarUrl()%>" alt="">
                            </div>
                            <div class="col-md-7 p-5">
                                <div class="row ">
                                    <p style=""> Rank: <%=userRank%><img src="img/rank.png" width="25px" height="25px" alt="alt"/></p>
                                </div>
                                <div class="row">
                                    <a class="" href="profile_detail">
                                        Edit info   
                                        <img style="width:35px;height:35px;" class="" src="img/edit.gif" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <hr>
                            <div class="button-container">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-6 ml-auto">
                                        <h5>12<br><small>Files</small></h5>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-6 ml-auto mr-auto">
                                        <h5><%=invoiceNumber%> invoice<br><small>Number</small></h5>
                                    </div>
                                    <div class="col-lg-3 mr-auto">
                                        <h5><%=totalMoney%> $<br><small>Spent</small></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h6 class="card-title">Menu</h6>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled team-members">
                                <!--  ----------------------------------------------------------------------------------- -->

                                <!-- -------------------------------------------------------------------------------------------------------- -->
                                <li>
                                    <div class="row" id="invoice">
                                        <div class="col-md-12  ">
                                            <div class="icon tittle row item_name m-2">
                                                <a class="" href="invoices">
                                                    <img src="img/bill.gif" style="width:35px;height:35px;border-radius: 10px" class="gif-icon" alt="Circle Image">   Invoices
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <!--  ----------------------------------------------------------------------------------- -->
                                <li>
                                    <div class="row">
                                        <div class="col-md-12  ">
                                            <div class="icon tittle row item_name  m-2">
                                                <a class="" href="medical_report">
                                                    <img src="img/report.png" style="width:35px;height:35px;border-radius: 10px" class="gif-icon" alt="Circle Image">   Medical Report
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row">
                                        <div class="col-md-12   ">
                                            <div class="icon tittle row item_name m-2">
                                                <a class="" href="reservation_history">
                                                    <img src="img/privilege.gif" style="width:35px;height:35px;border-radius: 10px" class="gif-icon" alt="Circle Image"> Reservation History
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <!--  ----------------------------------------------------------------------------------- -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <%
String service = (String) request.getAttribute("service");
String file = "";

switch (service) {
    case "edit_profile":
            %>
            <%@include file="user_edit.jsp" %>
            <%
            break;
    
        case "display_invoices":
        case "search_invoice":
            
            %>
            <%@include file="display_invoices.jsp" %>
            <%
            break;
    
        case "display_invoice_detail":
            %>
            <%@include file="invoice_detail.jsp" %>
            <%
            break;
    
        case "display_medical_report":
            %>
            <%@include  file="medical_report.jsp" %>
            <%
            break;
            
           case "display_reservations":
            %>
            <%@include  file="display_reservations.jsp" %>
            <%
            break;
        
    }
            %>

            <!-- Footer Start -->
            <%@include file="HealthCare/footer.jsp" %>
            <!-- Footer End -->

            <!--            <script>
                            var phoneInput = document.getElementById("phoneInput");
                            var phoneNumber = phoneInput.value;
                            var lastFourDigits = phoneNumber.slice(-6);
                            phoneInput.value = "******" + lastFourDigits;
                            //===============================================================
                            var emailInput = document.getElementById("emailInput");
                            var email = emailInput.value;
                            var headSubString = email.substring(0, 6);
                            var atIndex = email.indexOf("@");
                            var maskedEmail = headSubString + "****" + email.substring(atIndex);
                            emailInput.value = maskedEmail;
                        </script>-->


        </div>
    </body>

</html>