<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.User" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Medicine Management
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../assets/demo/demo.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <style>
            .table th, td{
                text-align: center;
            }
            #popup-update-background, #popup-add-background{
                display: none;
                width:100%;
                height:100%;
                position: fixed;
                z-index: 5000;
                background-color:rgba(120, 120, 120, 0.8);
            }
            .text-right{
                width: 230px;
            }
            #table-paging{
                text-align: center;
            }

            #table-paging a{
                font-size: 15px;
                padding-left: 5px;
            }
            .table button{
                padding: 10px 20px;
                font-size: 15px;
                background: #51cbce;
                color: white;
                border: none;
                outline: none;
                cursor: pointer;
                border-radius: 10px;
            }
            .popup{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width:500px;
                padding: 0px 40px;
                background: #FFFFFF;
                box-shadow: 2px 2px 5px 5px rgba(0,0,0,0.15);
                border-radius: 10px;
            }
            .popup .close-btn{
                position: absolute;
                top: 10px;
                right:10px;
                width:15px;
                height:15px;
                background:#888;
                color:#eee;
                text-align: center;
                line-height: 15px;
                border-radius: 15px;
                cursor: pointer;
            }
            .popup .form h2{
                text-align: center;
                color: #222;
                margin: 10px auto;
                font-size: 20px;
            }
            .popup .form .form-element{
                text-align: left;
                margin: 15px 0px;
            }
            .popup .form .form-element label{
                font-size:14px;
                color:#222;
            }
            .popup .form .form-element input, select{
                margin-top: 5px;
                display: block;
                width:100%;
                padding:10px;
                outline:none;
                border:1px solid #aaa;
                border-radius:5px;
            }
            .popup .form .form-element button{
                width:100%;
                height: 40px;
                border:none;
                outline:none;
                font-size:15px;
                background: #51cbce;
                color: #F5F5F5;
                border-radius: 10px;
                cursor: pointer;
            }
            .card-header button{
                width:100%;
                height: 40px;
                border:none;
                outline:none;
                font-size:15px;
                background: #51cbce;
                color: #F5F5F5;
                border-radius: 10px;
                cursor: pointer;
            }

        </style>
    </head>

    <body class="">
        <%User user=(User)session.getAttribute("user");%>
        <div class="wrapper ">
            <div class="sidebar" data-color="white" data-active-color="danger" >

                <div class="sidebar-wrapper" style="overflow: hidden">
                    <ul class="nav">
                        <li class="active">
                            <a href="profile_detail">
                                <i class="nc-icon nc-single-02"></i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li>
                            <a href="medicine">
                                <i class="nc-icon nc-tile-56"></i>
                                <p>Manage Medicine</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">

                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="javascript:;">User Profile</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navigation">
                            <form action="search" methos="post">
                                <div class="input-group no-border">
                                    <input type="text" oninput="searchByName(this)" value="${txt}" class="form-control">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <i class="nc-icon nc-zoom-split"></i>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="col-md-12 pt-5" style="padding:90px 50px 0px 50px !important" >
                    <div class="card card-user" style="box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3);">
                        <div class="card-header  ">
                            <h5 class="card-title text-center">Edit Profile <img  src="../img/edit.png" width="25px" height="25px" alt="alt"/></h5>
                        </div>
                        <div class="card-body">
                            <form action="../profile_update" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="service" value="update_profile"  >
                                <input type="hidden" name="cus_id" value="<%=user.getId()%>"  >
                                <div class="row col-md-12">                                        
                                    <div class="col-md-6 pr-1 form-group">
                                        <label>
                                            <h6>
                                                Email
                                            </h6>

                                        </label>
                                        <input name ="cus_email" id="emailInput" type="text" class="form-control" readonly placeholder=" email" value="<%=user.getEmail()%>">
                                    </div>
                                    <div class="col-md-6 px-1 form-group ">
                                        <label><h6>Name</h6></label>
                                        <input name="cus_name" type="text" class="form-control" placeholder="Enter full name" value="<%=user.getName()%>">
                                    </div>
                                </div>
                                <div class="row col-md-12">                                        
                                    <div class="col-md-6 pr-1 form-group">
                                        <label>
                                            <h6>Password
                                                <span style="color: #13c5dd;font-weight:300;">
                                                    <a href="password_change">(change)</a>
                                                </span>
                                            </h6>

                                        </label>
                                        <input name="cus_password" type="password" class="form-control" disabled="" placeholder="" value="<%=user.getPassword()%>" readonly>
                                    </div>
                                    <div class="col-md-6 px-1 form-group">
                                        <label>
                                            <h6>Phone
                                                <span style="color: #13c5dd;font-weight:300;">
                                                </span>
                                            </h6>

                                        </label>           
                                        <input name="cus_phone" id="phoneInput" type="phone" class="form-control" placeholder="" value="<%=user.getPhone()%>" readonly>
                                    </div>
                                </div>
                                <div class="row col-md-12">                                        
                                    <div class="form-group col-md-6 pr-1">
                                        <label><h6>Date of birth</h6></label>
                                        <input name="cus_dob"  type="date" class="form-control   " placeholder="Enter date of birth" value="<%=user.getDob()%>">
                                    </div>  
                                    <div class="col-md-6 px-1 form-group">
                                        <label><h6>Gender</h6></label>
                                        <select name="cus_gender" id="labels mb-0" class="form-control"  >
                                            <option value="male" <%=user.getGender().equalsIgnoreCase("Male") ? "selected" : "" %>>Male</option>
                                            <option value="female" <%=user.getGender().equalsIgnoreCase("Female") ? "selected" : "" %>>Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row col-md-12">                                        
                                    <div class="form-group col-md-6 pr-1">
                                        <label><h6>Address</h6></label>
                                        <input name="cus_address" type="text" class="form-control" placeholder="Enter address" value="<%=user.getAddress()%>">
                                    </div>
                                    <div class="form-group col-md-6 px-1">
                                        <label><h6>Chosen picture</h6></label>
                                        <input name="cus_photo" type="file" class="form-control " placeholder="Open file" >
                                    </div>                                         
                                </div>
                                <div class="row ">                                        
                                    <div class="update ml-auto mr-auto col-md-6 text-center mt-3">
                                        <button type="submit" class="btn btn-primary btn-round">Update Profile
                                        </button>
                                    </div>
                                    <div class=" reset ml-auto mr-auto col-md-6 text-center mt-3">
                                        <button class="btn btn-primary btn-round" type="reset">Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../assets/js/core/jquery.min.js"></script>
        <script src="../assets/js/core/popper.min.js"></script>
        <script src="../assets/js/core/bootstrap.min.js"></script>
        <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="../assets/js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src="../assets/demo/demo.js"></script>
    </body>

</html>