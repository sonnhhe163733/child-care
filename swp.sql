-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: childcare
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `admin_user_id` int NOT NULL,
  PRIMARY KEY (`admin_user_id`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`admin_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `answers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_id` int DEFAULT NULL,
  `answer_name` varchar(50) DEFAULT NULL,
  `answer_description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,1,'văn nam','hay quá','2023-07-09 17:00:00'),(2,1,'văn công','tuyệt vời','2023-07-09 17:00:00'),(3,5,'văn phong','câu hỏi hay','2023-07-10 02:41:01'),(4,10,'a','a','2023-07-12 06:04:34'),(5,10,'aaaa','aaaaa','2023-07-12 06:24:02');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) NOT NULL,
  `category_description` varchar(500) DEFAULT NULL,
  `category_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Examination','This menu provides professional and comprehensive examination services. Our healthcare team will conduct thorough examinations and diagnose your health condition. We use various tests and physical examinations to ensure accurate diagnosis and develop appropriate treatment plans.','https://cdn.benhvienthucuc.vn/wp-content/themes/tci-hospital/landing-page/page-kham-suc-khoe-cho-doanh-nghiep-4-2021/images/sec46_5_1.jpg'),(2,'Vaccination','This menu offers vaccination services to protect you against infectious diseases. We administer appropriate vaccines to build immunity and prevent the spread of dangerous diseases.','https://file.medinet.gov.vn//UploadImages/trungtamytethuduc/20200907_Tiem-chung-1.jpg?w=900'),(3,' ANALYSATION','This menu provides medical image analysis services. We utilize advanced imaging technology to evaluate and diagnose health issues. Our experts analyze images such as X-rays, ultrasounds, or MRIs to provide accurate diagnoses and assist in developing treatment plans.','https://benhvienthucuc.vn/wp-content/uploads/2020/11/chup-x-quang-la-gi-1.jpg'),(5,'EMERGENCY','This menu provides emergency services for urgent medical situations. Our professional healthcare team delivers immediate care and life-saving measures to handle critical emergencies.','https://cdn.luatvietnam.vn/uploaded/Images/Original/2021/01/06/20190715_041150_743230_thu-cam-on-cua-khac.max-1800x1800_0601094340.jpg'),(6,'EAR, NOSE AND THROAT','This menu offers services related to the examination, diagnosis, and treatment of conditions and ailments affecting the ears, nose, and throat. Our experienced medical professionals in the field of otolaryngology (ear, nose, and throat specialty) are dedicated to delivering comprehensive care and improving your overall ear, nose, and throat health.','https://cdn.bookingcare.vn/fr/w1000/2017/10/23/094305bac-si-tai-mui-hong-gioi.jpg'),(7,'EXTERNAL SYNTHETIC','The External Synthetic menu provides services related to external synthetic materials and devices used in medical treatments and procedures. Our specialized healthcare professionals are trained in utilizing external synthetic solutions to assist in various medical conditions and improve patient outcomes.','https://dcs-blog-wp.s3.ap-southeast-1.amazonaws.com/wp-content/uploads/2021/11/bac-si-tai-mui-hong-gioi-o-ha-noi-1.jpg'),(8,'Dentomaxillofacial','Odonto-Stomatology is a medical field concerned with the care and treatment of problems related to the teeth, jaw, and face.','http://ump.vnu.edu.vn/media/files/news/01(2).jpg');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certification`
--

DROP TABLE IF EXISTS `certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certification` (
  `certification_id` int NOT NULL AUTO_INCREMENT,
  `doctor_id` int NOT NULL,
  `certification_url` varchar(200) DEFAULT NULL,
  `certification_img_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`certification_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `certification_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctor_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certification`
--

LOCK TABLES `certification` WRITE;
/*!40000 ALTER TABLE `certification` DISABLE KEYS */;
/*!40000 ALTER TABLE `certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `comment_id` int NOT NULL AUTO_INCREMENT,
  `comment_text` longtext,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'a'),(2,'a'),(3,'aa'),(4,'aaa'),(5,'aaaa'),(6,'bach'),(7,'a'),(12,'bibibibi'),(13,'bibibibi'),(14,'toi la bach'),(15,'hehe\r\n'),(16,'bach'),(17,'hhhh'),(18,'to night'),(19,'tooo'),(20,'done\r\n'),(21,'oke done\r\n'),(22,NULL),(23,'he'),(24,NULL),(25,'ha'),(26,'bu'),(27,'a'),(28,'aaaa'),(29,'a'),(30,'hay');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `customer_user_id` int NOT NULL,
  `rank` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`customer_user_id`),
  CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`customer_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'gold'),(2,'kk');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `doctor_user_id` int NOT NULL,
  `doctor_description` text,
  PRIMARY KEY (`doctor_user_id`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`doctor_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (3,'Eye department '),(4,'Lungs Department '),(7,NULL),(8,NULL),(9,NULL),(11,'Description'),(12,NULL);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor_of_category`
--

DROP TABLE IF EXISTS `doctor_of_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor_of_category` (
  `doctor_id` int NOT NULL,
  `category_id` int NOT NULL,
  PRIMARY KEY (`doctor_id`,`category_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `doctor_of_category_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctor_user_id`),
  CONSTRAINT `doctor_of_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor_of_category`
--

LOCK TABLES `doctor_of_category` WRITE;
/*!40000 ALTER TABLE `doctor_of_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `doctor_of_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor_schedule`
--

DROP TABLE IF EXISTS `doctor_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor_schedule` (
  `doctor_schedule_id` int NOT NULL AUTO_INCREMENT,
  `doctor_id` int NOT NULL,
  `slot` char(1) NOT NULL,
  `number_of_reservation` tinyint NOT NULL DEFAULT '0',
  `day_of_week` int DEFAULT NULL,
  PRIMARY KEY (`doctor_schedule_id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `slot` (`slot`),
  CONSTRAINT `doctor_schedule_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctor_user_id`),
  CONSTRAINT `doctor_schedule_ibfk_2` FOREIGN KEY (`slot`) REFERENCES `slot` (`slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor_schedule`
--

LOCK TABLES `doctor_schedule` WRITE;
/*!40000 ALTER TABLE `doctor_schedule` DISABLE KEYS */;
INSERT INTO `doctor_schedule` VALUES (1,3,'1',3,2),(2,4,'3',3,2),(3,3,'2',2,2);
/*!40000 ALTER TABLE `doctor_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback` (
  `feedback_id` int NOT NULL AUTO_INCREMENT,
  `feedback_content` text,
  `feedback_grade` char(1) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,'Very good,satisfied with service','5'),(2,'Very nomal ,satisfied  ','4');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_of_invoice`
--

DROP TABLE IF EXISTS `feedback_of_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback_of_invoice` (
  `feedback_id` int NOT NULL,
  `invoice_id` int DEFAULT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `invoice_id` (`invoice_id`),
  CONSTRAINT `feedback_of_invoice_ibfk_1` FOREIGN KEY (`feedback_id`) REFERENCES `feedback` (`feedback_id`),
  CONSTRAINT `feedback_of_invoice_ibfk_2` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_of_invoice`
--

LOCK TABLES `feedback_of_invoice` WRITE;
/*!40000 ALTER TABLE `feedback_of_invoice` DISABLE KEYS */;
INSERT INTO `feedback_of_invoice` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `feedback_of_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import`
--

DROP TABLE IF EXISTS `import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `import` (
  `import_id` int NOT NULL AUTO_INCREMENT,
  `medicine_id` int NOT NULL,
  `wk_user_id` int NOT NULL,
  `medicine_import_quantity` int DEFAULT NULL,
  `import_date` date DEFAULT NULL,
  PRIMARY KEY (`import_id`),
  KEY `medicine_id` (`medicine_id`),
  KEY `wk_user_id` (`wk_user_id`),
  CONSTRAINT `import_ibfk_1` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`),
  CONSTRAINT `import_ibfk_2` FOREIGN KEY (`wk_user_id`) REFERENCES `warehouse_keeper` (`wk_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import`
--

LOCK TABLES `import` WRITE;
/*!40000 ALTER TABLE `import` DISABLE KEYS */;
/*!40000 ALTER TABLE `import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoice` (
  `invoice_id` int NOT NULL AUTO_INCREMENT,
  `invoice_customer_id` int NOT NULL,
  `invoice_doctor_schedule_id` int NOT NULL,
  `date_and_time` datetime DEFAULT NULL,
  `receptionist_id` int DEFAULT NULL,
  `reservation_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `invoice_customer_id` (`invoice_customer_id`),
  KEY `invoice_doctor_schedule_id` (`invoice_doctor_schedule_id`),
  KEY `receptionist_id` (`receptionist_id`),
  CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`invoice_customer_id`) REFERENCES `customer` (`customer_user_id`),
  CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`invoice_doctor_schedule_id`) REFERENCES `doctor_schedule` (`doctor_schedule_id`),
  CONSTRAINT `invoice_ibfk_3` FOREIGN KEY (`receptionist_id`) REFERENCES `nurse` (`nurse_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (1,1,1,'2023-07-02 10:00:00',5,'Phạm Trọng Đại '),(2,2,3,'2023-02-02 08:34:00',6,'Nguyễn Văn C'),(3,1,2,'2023-06-02 04:00:00',5,'Nguyẽn A'),(4,1,2,'2023-03-09 08:00:00',6,'B'),(5,1,3,'2023-08-02 09:00:00',5,'C'),(6,0,0,NULL,NULL,'D');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manager` (
  `manager_user_id` int NOT NULL,
  PRIMARY KEY (`manager_user_id`),
  CONSTRAINT `manager_ibfk_1` FOREIGN KEY (`manager_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (2),(3);
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_invoice`
--

DROP TABLE IF EXISTS `medical_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medical_invoice` (
  `invoice_id` int NOT NULL,
  `medicine_id` int NOT NULL,
  `medicine_quantity` int DEFAULT NULL,
  PRIMARY KEY (`invoice_id`,`medicine_id`),
  KEY `medicine_id` (`medicine_id`),
  CONSTRAINT `medical_invoice_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`),
  CONSTRAINT `medical_invoice_ibfk_2` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_invoice`
--

LOCK TABLES `medical_invoice` WRITE;
/*!40000 ALTER TABLE `medical_invoice` DISABLE KEYS */;
INSERT INTO `medical_invoice` VALUES (1,1,2),(1,2,1),(1,3,2),(2,2,1),(3,2,1),(4,2,1),(5,1,1),(5,3,1);
/*!40000 ALTER TABLE `medical_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_report`
--

DROP TABLE IF EXISTS `medical_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medical_report` (
  `medical_report_id` int NOT NULL AUTO_INCREMENT,
  `invoice_id` int NOT NULL,
  `symptoms` text,
  `conclusion` text,
  `service_id` int DEFAULT NULL,
  PRIMARY KEY (`medical_report_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `prk_service_id` (`service_id`),
  CONSTRAINT `medical_report_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`),
  CONSTRAINT `prk_service_id` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_report`
--

LOCK TABLES `medical_report` WRITE;
/*!40000 ALTER TABLE `medical_report` DISABLE KEYS */;
INSERT INTO `medical_report` VALUES (1,1,'The patient has symptoms of nasal obstruction on both sides of the nose, nasal pain and yellow nasal discharge.','After examining the nose, the doctor discovered obstruction of the nasal passages, signs of rhinitis and sinusitis. Conclusion may be acute rhinitis and sinusitis. Your doctor may recommend a sinus X-ray test to confirm the diagnosis and recommend treatment with antibiotics and pain relievers.',1),(2,2,'Dry cough, productive cough, shortness of breath, chest pain, fever, fatigue, weight loss, wheezing, difficulty swallowing, impaired physical activity,','There is a possibility of bronchitis, pneumonia, lung cancer, bacteria of respiratory infections, bacteria of throat infections, throat polyps, ... Note that',2),(3,1,'The patient has symptoms of sore throat, difficulty swallowing and dry cough.','Conclusion may be acute pharyngitis and tonsillitis. Your doctor may recommend simple tests such as a throat exam and suggest treatment with anti-inflammatory drugs, pain relievers, and a throat care regimen.',5),(4,3,'Abdominal pain, diarrhea, constipation, bleeding from the digestive tract, fatigue, weight loss, vomiting, heartburn, indigestion,..','Stomach: The gastric mucosa shows no signs of inflammation or damage. Small Intestine: The lining of the small intestine was free of abnormalities, and no polyps, ulcers, or abscesses were detected.',2),(5,4,'symptems1','have good general health. Children achieve basic developmental indicators such as weight and height within the normal range for their age',4),(7,5,'The patient presented with persistent sore throat, difficulty swallowing, and a small tumor in the pharynx.','The conclusion may be that the patient has a non-malignant tumor in the pharynx. Your doctor may recommend a throat ultrasound test and a specialist exam from an ENT specialist to determine the size and nature of the tumor and recommend appropriate treatment such as removal of the tumor or monitoring for growth. its.',5),(8,5,'The patient had persistent nasal congestion, runny nose, and nasal discharge for many months.','The conclusion may be that the patient has nasal polyps, a non-malignant tumor in the nose. Your doctor may recommend a nasal ultrasound test and a specialist exam from an ENT specialist to determine the size and nature of the polyp and recommend appropriate treatment such as medication or surgery.',1),(9,1,'Frequent symptoms of blurred vision or difficulty reading books or alphabets, along with headaches after reading for long periods of time.','Have symptoms of nearsightedness. Farsightedness is the inability to see distant objects clearly. Your doctor can diagnose presbyopia based on the results of a visual acuity test and measurement of myopia.',3);
/*!40000 ALTER TABLE `medical_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medicine` (
  `medicine_id` int NOT NULL AUTO_INCREMENT,
  `medicine_name` varchar(30) NOT NULL,
  `medicine_price` int NOT NULL,
  `medicine_quantity` int NOT NULL,
  `medicine_image` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` VALUES (1,'Dexamethasone',80,50,'https://cdn.youmed.vn/tin-tuc/wp-content/uploads/2020/06/thuoc-dexamethasone-1.jpg'),(2,'Bronchodilators',50,64,'https://nurseslabs.com/wp-content/uploads/2019/03/FT-Bronchodilators-Nursing-Pharmacology-768x552.png.webp'),(3,' Omeprazole',70,80,'https://vinmec-prod.s3.amazonaws.com/images/20210425_004632_025229_thuoc-Omeprazole.max-1800x1800.jpg');
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nurse`
--

DROP TABLE IF EXISTS `nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nurse` (
  `nurse_user_id` int NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nurse_user_id`),
  CONSTRAINT `nurse_ibfk_1` FOREIGN KEY (`nurse_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nurse`
--

LOCK TABLES `nurse` WRITE;
/*!40000 ALTER TABLE `nurse` DISABLE KEYS */;
INSERT INTO `nurse` VALUES (5,NULL,NULL),(6,NULL,NULL);
/*!40000 ALTER TABLE `nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `post_id` int NOT NULL AUTO_INCREMENT,
  `post_author_id` int DEFAULT NULL,
  `post_description` text NOT NULL,
  `post_created_date` date NOT NULL,
  `post_content` text NOT NULL,
  `post_source_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  KEY `post_author_id` (`post_author_id`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`post_author_id`) REFERENCES `manager` (`manager_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,'Khám bệnh là việc hỏi bệnh, khai thác tiền sử bệnh, thăm khám thực thể, khi cần thiết thì chỉ định làm xét nghiệm cận lâm sàng, thăm dò chức năng để chẩn đoán và chỉ định phương pháp điều trị phù hợp đã được công nhận.','2023-05-27','Khám bệnh','https://cdn.benhvienthucuc.vn/wp-content/uploads/2022/04/goi-kham-thuong-cho-tre-tu-0-6-tuoi-4-480x270.jpg'),(2,1,'Chữa bệnh là việc sử dụng phương pháp chuyên môn kỹ thuật đã được công nhận và thuốc đã được phép lưu hành để cấp cứu, điều trị, chăm sóc, phục hồi chức năng cho người bệnh.','2023-05-27','chữa bệnh','https://cdn.benhvienthucuc.vn/wp-content/uploads/2022/04/goi-kham-suc-khoe-tong-quat-dinh-ky-cho-thieu-nien-tu-16-den-duoi-18-tuoi-co-ban-1-480x270.jpg'),(3,1,'Bệnh sởi ở trẻ nhỏ có tốc độ lây lan nhanh và ảnh hưởng lớn tới sức khỏe của trẻ nên cần được phát hiện sớm và điều trị kịp thời. Vì vậy, nhận biết các dấu hiệu sởi ở trẻ từ sớm là điều mà bố mẹ nào cũng cần biết để hạn chế các biến chứ','2023-06-22','Bệnh sởi ở trẻ nhỏ: Dấu hiệu, nguyên nhân, cách điều trị','https://cdn.benhvienthucuc.vn/wp-content/uploads/2023/01/benh-soi-o-tre-nho-2-480x270.jpg'),(4,1,'Một trong vấn đề về sức khỏe mà trẻ em thường mắc phải chính là trào ngược dạ dày. Trào ngược không chỉ ảnh hưởng xấu đến sự sức khỏe của trẻ nhỏ mà còn dẫn tới nhiều hệ lụy nếu không điều trị đúng cách. Chính vì thế, cha mẹ cần nắm rõ dấu hiệu trẻ bị trào ngược dạ dày để chủ động nhận biết sớm bệnh và xử trí kịp thời.','2023-06-22','Dấu hiệu trẻ bị trào ngược dạ dày và cách xử trí','https://cdn.benhvienthucuc.vn/wp-content/uploads/2023/01/tre-bi-trao-nguoc-da-day-480x270.jpg');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_img`
--

DROP TABLE IF EXISTS `post_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post_img` (
  `post_img_id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `post_img_url` varchar(200) NOT NULL,
  `post_img_index` int NOT NULL,
  PRIMARY KEY (`post_img_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_img_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_img`
--

LOCK TABLES `post_img` WRITE;
/*!40000 ALTER TABLE `post_img` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `question_name` varchar(50) DEFAULT NULL,
  `question_email` varchar(50) DEFAULT NULL,
  `question_title` varchar(400) DEFAULT NULL,
  `question_description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (10,'2023-07-07 00:00:00','Duy Bách','haduybachbn@gmail.com','tôi bị đau chân, chân sưng to ở cổ chân','từ một tình huống diễn ra nhanh khi tôi đá bóng, tôi đã bị lật cổ chân ngay sau đó, tôi bị đau chân, mong ác sỹ hướng dẫn cách điều trị'),(11,'2023-07-08 00:47:51','John Smith','johnsmith@example.com','Migraine','John Smith experiences severe headaches accompanied by nausea and sensitivity to light and sound. The migraines occur at least twice a month and significantly impact his daily life.'),(12,'2023-07-08 00:48:23','Sarah Johnson','sarahjohnson@example.com','Asthma','Sarah Johnson suffers from recurring episodes of shortness of breath, wheezing, and chest tightness. These symptoms are triggered by exposure to allergens or physical exertion, making it difficult for her to engage in regular activities.'),(13,'2023-07-08 17:50:02','mohameth','mohameht@gmail.com','bị đau răng','tôi bị sâu răng, làm sao để hết đau'),(14,'2023-07-09 23:35:29','ndc','ndc@example.com','Question Title','Question Description'),(15,'2023-07-09 23:38:49','Nguyễn Xuân Minh','nguyenxuanminh@gmail.com','tôi bị đau răng','răng của tôi bị đau nhức. mong bác sỹ chỉ cách để tôi có thể hết đau nhức'),(16,'2023-07-11 22:17:56','đs','dsds','ewe','dsd');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservation` (
  `reservation_id` int NOT NULL AUTO_INCREMENT,
  `reservation_customer_id` int NOT NULL,
  `reservation_doctor_schedule_id` int NOT NULL,
  `status` varchar(20) NOT NULL,
  `date_and_time` date DEFAULT NULL,
  `reservation_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `reservation_customer_id` (`reservation_customer_id`),
  KEY `reservation_doctor_schedule_id` (`reservation_doctor_schedule_id`),
  CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`reservation_customer_id`) REFERENCES `customer` (`customer_user_id`),
  CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`reservation_doctor_schedule_id`) REFERENCES `doctor_schedule` (`doctor_schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (1,1,1,'Done','2023-07-02','Phạm Trọng Đại '),(2,2,1,'Process','2023-02-02','Nguyễn Văn A'),(3,1,2,'Done','2023-06-02','NGuyễn văn B'),(4,1,3,'Done','2023-03-09','Nguyễn Văn C'),(5,1,1,'Done','2023-08-02','ĐỖ Văn A'),(6,1,2,'Process','2023-07-11','NGuyễn Văn F'),(7,1,3,'Cancel','2020-03-02','A'),(8,1,1,'Process','2023-07-11','B'),(9,1,3,'Pending','2023-07-12','C'),(10,1,2,'Pending','2023-07-12','D'),(11,0,0,'',NULL,'E');
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `service_id` int NOT NULL AUTO_INCREMENT,
  `service_name` varchar(30) NOT NULL,
  `category_id` int NOT NULL,
  `service_price` int NOT NULL,
  `service_image` varchar(600) DEFAULT NULL,
  `service_description` varchar(600) DEFAULT NULL,
  `service_time` time DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `service_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Nose exam',6,123,'https://www.baosonhospital.com/Uploads/images/noi-soi-tai-mui-hong-cho-tre.jpg','A team of experienced ophthalmologists with deep expertise in ophthalmic care and providing a full range of medical and surgical eye care services','00:30:00'),(2,'Dental visit',8,212,'https://nhakhoaplatinum.com/wp-content/uploads/2020/09/kham-rang-cho-be-1.jpg','Painless endoscopy is a method of gastrointestinal endoscopy with anesthesia or pre-anesthesia before the endoscope is inserted into the body. ','00:30:00'),(3,'Ear examination',6,134,'https://www.baosonhospital.com/Uploads/images/noi-soi-tai-mui-hong.JPG','Pneumonia is an infection of the lungs. Therefore, when you see signs of shortness of breath, you need to go to the hospital to check.','00:30:00'),(4,'Jaw examination',8,234,'https://bookingcare.vn/files/image/2018/04/18/154417bac-si-kham-rang-cho-tre.jpg?w=800','This service provides routine check-ups and immunizations for children, helping to ensure health and prevent disease.','00:30:00'),(5,'Throat examination',6,235,'https://vinmec-prod.s3.amazonaws.com/images/20200514_123607_572447_screenshot_15894597.max-1800x1800.png','This service specializes in the management and treatment of childhood asthma, including assessment, treatment planning, and education for families and children.','00:30:00'),(6,'Pediatric Allergy',1,123,'https://cdn.tgdd.vn//News/0//benh-tu-mien-1-845x600.jpg','This service focuses on the diagnosis and treatment of allergies and related diseases of the immune system in children.','01:00:00'),(7,'Eyes test',1,200,'https://nld.mediacdn.vn/2014/e31kham-mat3-1402626163161.jpg','An eye exam is a process of checking and evaluating the health and function of the eyes. This process is done by an eye doctor (also known as an ophthalmologist or eye specialist).','00:30:00'),(8,'Blood tests',3,112,'https://benhvienungbuounghean.vn/wp-content/uploads/2018/07/xet-nghiem-mau-2241-614x400.jpg','Blood tests measure important blood serum parameters such as blood sugar, blood cell counts, liver and kidney function, levels of nutrients and fats, hormones',NULL),(9,'Urine test',3,100,'https://suckhoedoisong.qltns.mediacdn.vn/Images/duylinh/2020/07/06/MRTN-xet_nghim_nc_tiu_chn_oan_ung_th_TLT_resize.jpg','Urine tests measure parameters such as urine volume, color, pH, sugar, protein, impurities, waste, and signs of urinary system health.',NULL),(10,'Liver function test',3,140,'https://login.medlatec.vn//ImagePath/images/20211217/20211217_xet-nghiem-chuc-nang-gan-1.jpg','Liver function tests evaluate the functioning of the liver by checking indicators such as liver enzymes, bilirubin, liver proteins, and liver detoxification functions.',NULL),(11,'Flu vaccine ',2,234,'https://suckhoedoisong.qltns.mediacdn.vn/324455921873985536/2022/7/29/vaccine-ngua-cum-16590787180211448838489.jpg','Description Flu vaccine ',NULL),(12,'Measles vaccine',2,222,'https://cdn.youmed.vn/tin-tuc/wp-content/uploads/2021/05/vac-xin-quai-bi-3-768x512.jpg','Description Measles vaccine',NULL),(13,'Mumps vaccine',2,233,'https://yhoccongdong.com/Documents/1221_vaccin-soi,-quai-bi-va-rubella-(vaccin-mmr).jpg','Description Mumps vaccine',NULL),(14,'Synthetic Splints',7,440,'https://5.imimg.com/data5/SELLER/Default/2021/2/CT/UF/MQ/119952729/untitled-500x500.jpg','Synthetic cardboard is used to immobilize broken joints or bones.',NULL),(15,'Trauma Care',5,244,'https://newsmd2fr.keeng.net/tiin/archive/imageslead/2023/07/02/s4pfgffp0jsahzpzg3nef7tszvn402v3.jpg','Care of Severe Trauma: Includes diagnosis and treatment of patients in accidents, injuries from traffic accidents, work-related accidents, and other hazardous situations.',NULL);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_doctor`
--

DROP TABLE IF EXISTS `service_doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_doctor` (
  `service_id` int NOT NULL,
  `doctor_user_id` int NOT NULL,
  PRIMARY KEY (`service_id`,`doctor_user_id`),
  KEY `doctor_user_id` (`doctor_user_id`),
  CONSTRAINT `service_doctor_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`),
  CONSTRAINT `service_doctor_ibfk_2` FOREIGN KEY (`doctor_user_id`) REFERENCES `doctor` (`doctor_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_doctor`
--

LOCK TABLES `service_doctor` WRITE;
/*!40000 ALTER TABLE `service_doctor` DISABLE KEYS */;
INSERT INTO `service_doctor` VALUES (1,3),(3,3),(5,3),(2,4),(4,4),(6,7),(7,7),(8,8),(9,8),(10,8),(15,9),(11,11),(12,11),(13,11),(14,11),(1,12);
/*!40000 ALTER TABLE `service_doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_invoice`
--

DROP TABLE IF EXISTS `service_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_invoice` (
  `invoice_id` int NOT NULL,
  `service_id` int NOT NULL,
  PRIMARY KEY (`invoice_id`,`service_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_invoice_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`),
  CONSTRAINT `service_invoice_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_invoice`
--

LOCK TABLES `service_invoice` WRITE;
/*!40000 ALTER TABLE `service_invoice` DISABLE KEYS */;
INSERT INTO `service_invoice` VALUES (1,1),(5,1),(2,2),(3,2),(1,3),(4,4),(1,5),(5,5);
/*!40000 ALTER TABLE `service_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_reservation`
--

DROP TABLE IF EXISTS `service_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_reservation` (
  `reservation_id` int NOT NULL,
  `service_id` int NOT NULL,
  PRIMARY KEY (`reservation_id`,`service_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_reservation_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`reservation_id`),
  CONSTRAINT `service_reservation_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_reservation`
--

LOCK TABLES `service_reservation` WRITE;
/*!40000 ALTER TABLE `service_reservation` DISABLE KEYS */;
INSERT INTO `service_reservation` VALUES (1,1),(5,1),(7,1),(3,2),(9,2),(1,3),(10,3),(4,4),(9,4),(1,5),(5,5),(7,5),(6,6),(8,6);
/*!40000 ALTER TABLE `service_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slot`
--

DROP TABLE IF EXISTS `slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slot` (
  `slot_id` char(1) NOT NULL,
  `slot_start_time` time DEFAULT NULL,
  `slot_end_time` time DEFAULT NULL,
  PRIMARY KEY (`slot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slot`
--

LOCK TABLES `slot` WRITE;
/*!40000 ALTER TABLE `slot` DISABLE KEYS */;
INSERT INTO `slot` VALUES ('1','07:30:00','09:50:00'),('2','10:00:00','12:20:00'),('3','12:50:00','03:10:00'),('4','03:20:00','05:30:00');
/*!40000 ALTER TABLE `slot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_email` varchar(30) NOT NULL,
  `user_password` varchar(20) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_phone` varchar(12) NOT NULL,
  `user_gender` varchar(6) NOT NULL,
  `user_dob` date NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `user_role` varchar(10) NOT NULL,
  `user_avatar_url` varchar(400) DEFAULT NULL,
  `account_type` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  UNIQUE KEY `user_phone` (`user_phone`),
  CONSTRAINT `user_chk_1` CHECK (((`user_gender` = _utf8mb4'Male') or (`user_gender` = _utf8mb4'Female')))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'soncrt1234@gmail.com','Songuyen27062k2','sonds','03242332','male','2002-09-06','Ha Nam-VN-dsds','1','https://haycafe.vn/wp-content/uploads/2021/11/hinh-anh-hoat-hinh-de-thuong-cute-dep-nhat.jpg','email'),(2,'sonnhhe163733@fpt.edu.vn','','son2','','male','2023-02-09','Ha Nam-VN','1','https://haycafe.vn/wp-content/uploads/2021/11/hinh-anh-hoat-hinh-de-thuong-cute-dep-nhat.jpg','google'),(3,'doctor1@gmail.com','270602','doctor1','0689679002','male','2000-02-09','Ha Nam-VN','3','https://hips.hearstapps.com/hmg-prod/images/portrait-of-a-happy-young-doctor-in-his-clinic-royalty-free-image-1661432441.jpg?crop=0.66698xw:1xh;center,top&resize=1200:*',NULL),(4,'doctor2@gmail.com','270602','doctor2','036967002','male','1990-02-09','Ha Nam-VN','3','https://images.theconversation.com/files/304957/original/file-20191203-66986-im7o5.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip',NULL),(5,'nurse1@gmail.com','270602','nurrse1','043874384743','female','1980-09-23','Ha Tinh','2',NULL,NULL),(6,'nurse2@gmail.com','270602','nurse2','434434984','male','1978-08-03','Ha Tay','2',NULL,NULL),(7,'doctor3@gmail.com','44545','doctor3','54554545','male','2023-09-08','HN','3','https://familydoctor.org/wp-content/uploads/2018/02/41808433_l-705x470.jpg',NULL),(8,'doctor4@gmail.com','8787878','doctor4','43343434','male','2022-06-04','Ninh Binh','3','https://media.istockphoto.com/id/1189304032/vi/anh/b%C3%A1c-s%C4%A9-c%E1%BA%A7m-m%C3%A1y-t%C3%ADnh-b%E1%BA%A3ng-k%E1%BB%B9-thu%E1%BA%ADt-s%E1%BB%91-t%E1%BA%A1i-ph%C3%B2ng-h%E1%BB%8Dp.jpg?s=612x612&w=0&k=20&c=ykEnHCE8T7DQhqUPFK0uy9KMGSU45pxl6uWuIw_vi_c=',NULL),(9,'doctor5@gmail.com','434344','doctor5','5454545','male','2000-06-03','Ninh Thuan','3','https://www.siasat.com/wp-content/uploads/2023/04/Dr-Sudhir-Kumar.png',NULL),(10,'minhpthe171392@fpt.edu.vn','Theminh1770!','minhpthe171392@fpt.edu.vn','0982469362','Male','2001-07-17','Ha Noi','1',NULL,NULL),(11,'doctor6@gmail.com','4343','doctor6','54545454','Female','2002-07-17','Phu Tho','3',NULL,NULL),(12,'doctor7@gmail.com','545','doctor7','454545','male','2002-07-17','Ha Nam','3',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_keeper`
--

DROP TABLE IF EXISTS `warehouse_keeper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse_keeper` (
  `wk_user_id` int NOT NULL,
  PRIMARY KEY (`wk_user_id`),
  CONSTRAINT `warehouse_keeper_ibfk_1` FOREIGN KEY (`wk_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse_keeper`
--

LOCK TABLES `warehouse_keeper` WRITE;
/*!40000 ALTER TABLE `warehouse_keeper` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_keeper` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-26 13:04:37
