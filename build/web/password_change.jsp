<!DOCTYPE html>
<html>
    <head>
        <title>Password Change</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">   
        <style>
            body {
                background-image: url("img/login_bgr.png");
                font-family: Arial, sans-serif;
                background-size: cover;
            }

            .container {
                max-width: 600px;
                margin: 70px auto;
                padding: 40px 40px;
                background-color:#ffffff;
                border-bottom: solid #666666 2px;
                border-right: solid #666666 2px;
                border-radius: 30px;
                box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.911);
            }

            .form-group {
                margin-bottom: 20px;
            }

            .form-group label {
                display: block;
                font-weight: bold;
                margin-bottom: 5px;
                color: #13c5dd;
            }

            .form-group input[type="email"],
            .form-group input[type="password"] {
                width: 100%;
                padding: 10px 30px;
                border-radius: 10px;
                border: none;
                outline: none;
                border-bottom: 2px solid #848e9f;
                color: #13c5dd;
            }

            .form-group input[type="email"]:focus,
            .form-group input[type="password"]:focus {
                border-bottom: 2px solid #13c5dd;
                color:#666666 ;
                border:solid #13c5dd 2px;
            }

            .form-group input[type="submit"] {
                width: 100%;
                padding: 10px;
                border-radius: 10px;
                background-color: #13c5dd;
                color: #ffffff;
                border: none;
                cursor: pointer;

            }

            .form-group a {
                text-decoration: none;
            }
            .icon{
                border:solid #13c5dd 2px;
                border-radius:10px;
                background-color: #13c5dd;

            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </head>
    <body >
        <div class="container">
            <h2 class="text-center" style="color: #13c5dd;">Change Password</h2>
            <form action="password_change" method="post">
                <div class="form-group">
                    <label for="inputPass">Current Password:</label>
                    <input class="mr-5 bg-transparent" type="password" 
                           id="inputPass" name="inputPass" required>
                    <div id="pass-1" style="color: #842029">${inputMess}</div>
                </div>
                <div class="form-group">
                    <label for="newPass">New Password:</label>
                    <input class="mr-5 bg-transparent" type="password" 
                           oninput="checkPass(newPass, rePass)" value="${txt}" 
                           id="newPass" name="newPass" required>
                    <div id="newMess" hidden style="color: red">Password is not valid.
                        Password must contain at least 1 uppercase character, 
                        1 digit and 1 special letter.</div>
                </div>
                <div class="form-group">
                    <label for="rePass">Confirm Password:</label>
                    <input class="mr-5 bg-transparent" type="password" 
                           oninput="checkPass(newPass, rePass)" value="${txt}" 
                           id="rePass" name="rePass" required>
                    <div id="reMess" hidden style="color: red">Re-password must 
                        be similar to new password.</div>
                </div>
                <div class="form-group">
                    <input id="button-change" type="submit" value="Change" disabled>
                </div>
            </form>
        </div>
        <script>
            function checkPass(newPass, rePass) {
                var newPass = newPass.value;
                var rePass = rePass.value;
                $.ajax({
                    url: "password_change_ajax",
                    type: "get",
                    data: {
                        newTxt: newPass,
                        reTxt: rePass
                    },
                    success: function (data) {
                        var newMess = document.getElementById("newMess");
                        var reMess = document.getElementById("reMess");
                        newMess.hidden = true;
                        reMess.hidden = true;
                        const mess = data.split(" ");
                        var m = 0;
                        for (var i = 0; i < mess.length; i++) {
                            if (mess[i] === "newMess") {
                                newMess.hidden = false;
                                m += 1;
                            }
                            if (mess[i] === "reMess") {
                                reMess.hidden = false;
                                m += 1;
                            }
                        }
                        if (m === 0)
                            document.getElementById("button-change").disabled = false;
                    }
                });
            }
        </script>
    </body>
</html>