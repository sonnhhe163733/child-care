<%--
    Document   : popup
    Created on : May 22, 2023, 4:57:55 PM
    Author     : Son Nguyen
--%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        .custom-alert {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 9999;
        }

        .custom-alert-content {
            background: #f6f8f9db;
            padding: 30px 20px;
            border-radius: 10px;
            text-align: center;
            box-shadow: 30px 30px 30px rgba(148, 147, 147, 0.3);
        }

        .custom-alert-title {
            font-size: 20px;
            margin-bottom: 10px;
        }

        .custom-alert-message,
        .custom-alert-title {
            margin-bottom: 20px;
            color: #13c5dd;
        }

        .custom-alert-button {
            padding: 8px 20px;
            border: none;
            background: #13c5dd;
            color: white;
            border-radius: 10px;
            cursor: pointer;
            box-shadow: 30px 30px 70px rgba(148, 147, 147, 0.5);
        }
    </style>
    <script>
        function showPopup(showCancel) {
            var customAlert = document.getElementById('custom-alert');
            customAlert.style.display = 'flex';

            var cancelButton = document.getElementById('cancel-button');
            cancelButton.style.display = showCancel ? 'inline-block' : 'none';
        }

        function closeCustomAlert() {
            var customAlert = document.getElementById('custom-alert');
            customAlert.style.display = 'none';
        }

        function confirmAction() {
            // Perform the desired action here
            console.log('Action confirmed');
            closeCustomAlert();
        }

        function cancelAction() {
            console.log('Action canceled');
            closeCustomAlert();
        }
    </script>
</head>
<body>
    <%String mess=(String)request.getAttribute("mess");%>
    <%if(mess!=null){%>
    <div id="custom-alert" class="custom-alert">
        <div class="custom-alert-content">
            <img style="width:20px;height: 20px" src="img/alert.png" alt="alert" /> <h3 class="custom-alert-title">Alert</h3>
            <p class="custom-alert-message"><%=mess%></p>
            <button class="custom-alert-button" onclick="confirmAction()">OK</button>
        </div>
    </div>
    <%}%>
</body>
</html>
