package dao.ManagerDAO;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import dao.ManagerDAO.ManagerDAO;
import dao.post.PostListDAO;
import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Post;

/**
 *
 * @author LT
 */
public class ManagerDAO {
     Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    
    
     public List<Post> getPostList() {
        List<Post> list = new ArrayList<>();
        String query = "SELECT * FROM  childcare.post";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Post(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3).substring(0, 50)+"...",
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                )
                );
            }
        } catch (Exception e) {
            System.out.println("getPostList: "+e.getMessage());
        }

        return list;
    }
     
     public void insertPost(String content, String description, String image){
        String query = "INSERT INTO post (post_content, post_description, post_source_url, post_created_date) VALUES (?, ?, ?, \"2023/7/19\");";
        try {
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, content);
             ps.setString(2, description);
             ps.setString(3, image);
           
             ps.executeUpdate();
        } catch (Exception e) {
        }
    }
      public void delete(String pid){
        String query = "delete from post where post_id = ?";
                try {
            conn = new DBContext().connection;
             ps = conn.prepareStatement(query);
             ps.setString(1, pid);
             ps.executeUpdate();
        } catch (Exception e) {
                    System.out.println(e.getMessage());
        }
    }
     
//     public static void main(String[] args) {
//        PostListDAO dao = new PostListDAO();
//        List<Post> list = dao.getPostList();
//         for (Post o : list) {
//               System.out.println(o);     
//         }
//            
//            
//        
//    }
      public static void main(String[] args) {
    // Khởi tạo đối tượng của lớp chứa hàm delete()
    ManagerDAO obj = new ManagerDAO();
    obj.insertPost("ádsad", "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss", "sadá");
    // Gọi hàm delete() và truyền giá trị pid để xóa bài viết có post_id tương ứng


    
}
}
