/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.medicineDAO;

import dbcontext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Medicine;

/**
 *
 * @author Admin
 */
public class MedicineDAO extends DBContext {

    Connection cnn;
    PreparedStatement pstm;
    ResultSet rs;

    public void addMedicine(Medicine med) {
        try {
            String strAdd = "insert into medicine"
                    + "(medicine_name,medicine_unit,medicine_price,medicine_quantity)"
                    + " values (?,?,?,?)";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strAdd);
            pstm.setString(1, med.getName());
            pstm.setString(2, med.getUnit());
            pstm.setDouble(3, med.getPrice());
            pstm.setInt(4, med.getQuantity());
            pstm.execute();
        } catch (Exception e) {
            System.out.println("MedicineAdd: " + e.getMessage());
        }
    }

    public void updateMedicine(Medicine med) {
        try {
            String strUpdate = "update medicine set medicine_name=?, medicine_unit=?, "
                    + "medicine_price= ?, medicine_quantity=? where medicine_id=?";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strUpdate);
            pstm.setString(1, med.getName());
            pstm.setString(2, med.getUnit());
            pstm.setDouble(3, med.getPrice());
            pstm.setInt(4, med.getQuantity());
            pstm.setInt(5, Integer.parseInt(med.getId()));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("MedicineUpdate: " + e.getMessage());
        }
    }

    public void deleteMedicine(Medicine med) {
        try {
            String strDelete = "delete from medicine where medicine_id=?";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strDelete);
            pstm.setInt(1, Integer.parseInt(med.getId()));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("MedicineDelete: " + e.getMessage());
        }
    }
    
    public int getTotalAccount(String txtSearch) {
        try {
            String strSelect = "select count(medicine_id)"
                    + "from medicine where medicine_name like ? ";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, "%" + txtSearch + "%");
            rs = pstm.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getInt(1));
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("getTotalAccount: " + e.getMessage());
        }
        return 0;
    }
    
    public List<Medicine> SearchByName(String txtSearch, int index) {
        List<Medicine> medList = new ArrayList<>();
        try {
            String strSelect = "select medicine_id, medicine_name, "
                    + "medicine_unit, medicine_quantity, medicine_price "
                    + "from medicine where medicine_name like ? "
                    + "order by ? limit ? , ? ";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, "%" + txtSearch + "%");
            pstm.setString(2, "medicine_id");
            pstm.setInt(3, (index-1)*5);
            pstm.setInt(4, 5);
            System.out.println(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                medList.add(new Medicine(rs.getString(1), rs.getString(2),
                        rs.getString(3), Integer.parseInt(rs.getString(4)),
                        Integer.parseInt(rs.getString(5))));
            }
            System.out.println(medList);
        } catch (Exception e) {
            System.out.println("SearchByName: " + e.getMessage());
        }
        return medList;
    }

    public List<Medicine> getListMedicine() {
        List<Medicine> medList = new ArrayList<>();
        try {
            System.out.println("connect!");
            String strSelect = "select medicine_id, medicine_name, "
                    + "medicine_unit, medicine_quantity, medicine_price "
                    + "from medicine";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while(rs.next()){
                medList.add(new Medicine(rs.getString(1),rs.getString(2),
                        rs.getString(3),Integer.parseInt(rs.getString(4)), 
                        Integer.parseInt(rs.getString(5))));
            }
            System.out.println(medList);
        } catch (Exception e) {
            System.out.println("MedicineList: " + e.getMessage());
        }
        return medList;
    }

}
