/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.reservation;

import dao.service.ServiceListDAO;
import dao.user.ProfileDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Reservation;
import model.Service;

/**
 *
 * @author Son Nguyen
 */
public class ReservationListDAO extends dbcontext.DBContext {

    public List<Reservation> getReservationsByCusID(String cusID) {
        ServiceListDAO servicesDAO = new ServiceListDAO();
        List<Reservation> reservations = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement("select  r.reservation_id, uc.user_name,uc.user_dob,uc.user_phone,uc.user_email,\n"
                    + "  ud.user_id as doctor_id ,ud.user_name as doctor_name ,\n"
                    + "  sl.slot_start_time,sl.slot_end_time,r.date_and_time,\n"
                    + "  r.status\n"
                    + "from reservation as r inner join user as uc \n"
                    + "on uc.user_id= r.reservation_customer_id inner join doctor_schedule as ds\n"
                    + "on r.reservation_doctor_schedule_id=ds.doctor_schedule_id inner join user as ud\n"
                    + "on ds.doctor_id=ud.user_id inner join slot as sl \n"
                    + "on sl.slot_id=ds.slot where r.reservation_customer_id= ?");
            pre.setString(1, cusID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                reservations.add(new Reservation(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                        servicesDAO.getServicesByReservationID(rs.getString(1)),
                        rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getString(9), rs.getDate(10),
                        rs.getString(11)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reservations;
    }

    public List<Reservation> searchReservationsByCusIDAndServiceNameOrDoctorName(String cusID, String searchName) {
        model.ReservationList reservations = new model.ReservationList(getReservationsByCusID(cusID));
        model.ReservationList reservationsSearchByServiceNameOrDoctorName = new model.ReservationList();
        for (Reservation r : reservations.getReservations()) {
            if (r.getDocName().contains(searchName)) {
                reservationsSearchByServiceNameOrDoctorName.addReservation(r);
            }
            for (Service s : r.getServices()) {
                if (s.getName().contains(searchName)) {
                    reservationsSearchByServiceNameOrDoctorName.addReservation(r);
                }
            }
        }
        return reservationsSearchByServiceNameOrDoctorName.getReservations();
    }

    public List<Reservation> getReservationaByCusIDAndStatus(String cusID, String status) {
        model.ReservationList reservations = new model.ReservationList(getReservationsByCusID(cusID));
        model.ReservationList reservationsSearchByStatus = new model.ReservationList();
        if (status.equals("All")) {
            return reservations.getReservations();
        }
        for (Reservation r : reservations.getReservations()) {
            if (r.getStatus().equals(status)) {
                reservationsSearchByStatus.addReservation(r);
            }
        }
        return reservationsSearchByStatus.getReservations();

    }
}
