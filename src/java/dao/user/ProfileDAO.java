/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.MedicineItem;
import model.User;

/**
 *
 * @author Son Nguyen
 */
public class ProfileDAO extends dbcontext.DBContext {

    public int updateProfile(User user) throws SQLException {
    int n = 0;
    PreparedStatement pre = connection.prepareStatement("UPDATE `childcare`.`user`\n"
            + "SET\n"
            + "`user_name` = ?,\n"
            + "`user_gender` = ?,\n"
            + "`user_dob` = ?,\n"
            + "`user_address` = ?\n"
//            + "`user_avatar_url` = ?\n"
            + "WHERE `user_id` = ?;");
    
    pre.setString(1, user.getName());
    pre.setString(2, user.getGender());
    pre.setString(3, user.getDob());
    pre.setString(4, user.getAddress());
//    pre.setString(5, user.getAvatarUrl());
    pre.setString(5, user.getId());

    n = pre.executeUpdate();
    return n;
}


    public String getRankByCustomerId(String cusID) {
        String rank = "Tiem nang";
        try {
            PreparedStatement pre = connection.prepareStatement("SELECT `rank` FROM `customer` WHERE `customer_user_id` = ?");
            pre.setString(1, cusID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                rank = rs.getString("rank");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rank;
    }

}
