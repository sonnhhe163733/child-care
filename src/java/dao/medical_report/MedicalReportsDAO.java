/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.medical_report;

import dao.user.ProfileDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.MedicalReport;
import model.Service;

/**
 *
 * @author Son Nguyen
 */
public class MedicalReportsDAO extends dbcontext.DBContext{
    public List<MedicalReport> getMedicalReportListByCusID(String cusID) {
        Hashtable<String, MedicalReport> reports = new Hashtable<>();
        model.MedicalReportList medicalReports = new model.MedicalReportList();
        try {
            PreparedStatement pre = connection.prepareStatement(" SELECT mr.medical_report_id, mr.symptoms, mr.conclusion,\n"
                    + "                    s.service_id,i.date_and_time FROM invoice AS i\n"
                    + "                    INNER JOIN service_invoice AS si ON i.invoice_id = si.invoice_id\n"
                    + "                    INNER JOIN service AS s ON si.service_id = s.service_id \n"
                    + "                    inner join medical_report as mr on mr.invoice_id=i.invoice_id and mr.service_id=s.service_id\n"
                    + "                   WHERE i.invoice_customer_id = ?  ;");
            pre.setString(1, cusID);

            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                if (reports.get(rs.getString(4)) != null) {
                    if (reports.get(rs.getString(4)).getDateAndTime().compareTo(rs.getDate(5)) > 0) {
                        reports.put(rs.getString(4), reports.get(rs.getString(4)));
                    }
                }
                reports.put(rs.getString(4), new MedicalReport(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(5)));
            }
            for (String serID : reports.keySet()) {
                medicalReports.addReport(reports.get(serID));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medicalReports.getReports();
    }
    public List<MedicalReport> getMedicalReportsByCusIDAndInvoiceID(String cusID, String invoiceID) {
        List<MedicalReport> medicalReports = new ArrayList<>();
        try {
            PreparedStatement pre = connection.prepareStatement(" SELECT mr.medical_report_id, mr.symptoms, mr.conclusion,\n"
                    + "                    s.service_id, s.service_name, s.service_price, s.service_image,s.service_time FROM invoice AS i\n"
                    + "                    INNER JOIN service_invoice AS si ON i.invoice_id = si.invoice_id\n"
                    + "                    INNER JOIN service AS s ON si.service_id = s.service_id \n"
                    + "                    inner join medical_report as mr on mr.invoice_id=i.invoice_id and mr.service_id=s.service_id\n"
                    + "                   WHERE i.invoice_customer_id = ? and i.invoice_id= ? ;");
            pre.setString(1, cusID);
            pre.setString(2, invoiceID);

            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                medicalReports.add(new MedicalReport(rs.getString(1), rs.getString(2), rs.getString(3),
                        new Service(rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8))));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medicalReports;
    }
}
