/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.reservation.ReservationListDAO;
import dao.user.ProfileDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import model.Reservation;
import model.Service;
import model.User;

/**
 *
 * @author Son Nguyen
 */
@WebServlet(name = "SearchOrFilterReservationController", urlPatterns = {"/search_or_filter_reservations"})
public class SearchOrFilterReservationController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            String cusID = user.getId();
            String searchName = request.getParameter("search_name");
            String reservationStatus = request.getParameter("reservation_status");
            ReservationListDAO dao = new ReservationListDAO();
            List<Reservation> reservations;
            if (searchName != null) {
                if (searchName.isEmpty()) {
                    reservations = dao.getReservationsByCusID(cusID);
                } else {
                    reservations = dao.searchReservationsByCusIDAndServiceNameOrDoctorName(cusID, searchName);
                }
            } else {
                reservations = dao.getReservationaByCusIDAndStatus(cusID, reservationStatus);
            }
            if (!reservations.isEmpty()) {
                out.println("  <div class=\"container bg-trasparent pb-3 p-2  m-1 pt-0\" id=\"reservations\"> \n"
                        + "\n"
                        + "\n");
                for (Reservation re : reservations) {

                    out.print("           <div class=\"card h-100 shadow-sm row mb-4\" style=\"position: relative;box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3);\">\n"
                            + "                    <div class=\"card-header text-center\" >\n"
                            + "                        <a href=\"invoice_detail?invoice_id=" + re.getReservationID() + "\">\n"
                            + "                            <span style=\"color: #1d2a4d; font-weight: 600; font-size: 20px; \">\n"
                            + "                                Reservation ID: " + re.getReservationID() + "\n"
                            + "                            </span>\n"
                            + "                            <span style=\"color: #ff6666; font-weight: 600; font-size: 20px; \">\n"
                            + "                               ( " + re.getStatus() + ")"
                            + "                            </span>\n"
                            + "                        </a>\n"
                            + "                    </div>\n"
                            + "\n");
                    double total = 0;
                    for (Service s : re.getServices()) {
                        out.print("               <div class=\"row col-md-11  \"  style=\"height:70px;border-bottom:2px solid #eceaea;margin: 0px 35px \">\n"
                                + "                        <div class=\"col-md-3 p-2 text-center\">\n"
                                + "\n"
                                + "                            <img src=\"" + s.getImage() + "\" width=\"85px\" height=\"50px\" style=\"border-radius:10px;\"  />\n"
                                + "                        </div>\n"
                                + "\n"
                                + "                        <div class=\"col-md-6 p-2 text-center\" >\n"
                                + "                            <div class=\"item_name \">  <h7 style=\"color: #1d2a4d;font-weight:600\"> " + s.getName() + "</h7> </div>\n"
                                + "                        </div>\n"
                                + "\n"
                                + "\n"
                                + "\n"
                                + "\n"
                                + "\n"
                                + "                        <div class=\"col-md-3 p-2 text-center \">\n"
                                + "                            <p style=\"color: #1d2a4d;text-align:center;font-weight: 600\">\n"
                                + "                                Service " + s.getId() + ":\n"
                                + "                                <span style=\"color: #ff6666; font-size: 20px; font-weight: 600;\">" + s.getPrice() + " $</span>\n");
                        total += Double.parseDouble(s.getPrice());
                        out.print("                       </p>\n"
                                + "                        </div>\n"
                                + "                    </div>\n");
                    }
                    out.print("\n"
                            + "                    <div class=\"col-md-12 pb-1 card-footer \">\n"
                            + "\n"
                            + "                        <p style=\"color: #1d2a4d;text-align:right;font-weight: 600\">\n"
                            + "                            <img src=\"img/cost.png\" width=\"40px\" height=\"40px\" alt=\"alt\"/>TotaL Money: \n"
                            + "                            <span style=\"color: #ff6666; font-size: 25px; font-weight: 600;margin-right: 12px;\">" + total + " $</span>\n"
                            + "                        </p>\n"
                            + "\n"
                            + "                    </div>\n"
                            + "\n"
                            + "                    <div class=\"col-md-12 row justify-content-center\">\n");
                    if (re.getStatus().equals("Done")) {
                        out.print("                        <div class=\"col-md-3 text-center item_name m-2 p-2\">\n"
                                + "                            <a href=\"profile?s?evaluated_s\">\n"
                                + "                                <img style=\"border: solid #999999 2px; border-radius: 10px;\" src=\"img/evalute.gif\" width=\"40px\" height=\"40px\" alt=\"alt\" />\n"
                                + "                                Evaluate\n"
                                + "                            </a>\n"
                                + "                        </div>\n");
                    }
                    out.print("                        <div class=\"col-md-3 text-center item_name m-2 p-2\">\n"
                            + "                            <a href=\"profile?s?rebook_s\">\n"
                            + "                                <img style=\"border: solid #999999 2px; border-radius: 10px;\" src=\"img/booking.gif\" width=\"40px\" height=\"40px\" alt=\"alt\" />\n"
                            + "                                Rebook\n"
                            + "                            </a>\n"
                            + "                        </div>\n"
                            + "                        <div class=\"col-md-3 text-center item_name m-2 p-2\">\n"
                            + "                            <a href=\"mailto:soncrt1234@gmail.com\">\n"
                            + "                                <img style=\"border: solid #999999 2px; border-radius: 10px;\" src=\"img/mail.gif\" width=\"40px\" height=\"40px\" alt=\"alt\" />\n"
                            + "                                Send email\n"
                            + "                            </a>\n"
                            + "                        </div>\n"
                            + "                    </div>\n"
                            + "\n"
                            + "                </div>\n"
                            + "\n");
                }
                out.print("\n"
                        + "            </div>");
            } else {
                out.print(
                        "            <div style=\"display:block;background:white;margin:auto;box-shadow:10px 10px 10px rgba(148, 147, 147, 0.5);border-radius:10px;padding:200px;height:550px;\">\n"
                        + "                <div>"
                        + "                    <h5 style=\"text-align: center;color:#13c5dd;display:block;\">Not find invoice!</h5>\n"
                        + "                    <img src=\"img/not_find.png\" width=\"150px\" height=\"150px\" style=\"margin:2px 210px\">\n"
                        + "                </div>"
                        + "            </div>\n");
            }
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
