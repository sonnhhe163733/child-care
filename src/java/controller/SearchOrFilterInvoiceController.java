/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.invoice.InvoiceListDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import model.Invoice;
import model.MedicineItem;
import model.MedicalReport;
import model.Service;
import model.User;

/**
 *
 * @author Son Nguyen
 */
@WebServlet(name = "SearchAndFilterInvoiceController", urlPatterns = {"/search_or_filter_invoices"})
public class SearchOrFilterInvoiceController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            List<Invoice> invoices;
            InvoiceListDAO dao = new InvoiceListDAO();
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            String cusID = user.getId();
            String searchName = request.getParameter("search_name");
            String sortPrice = request.getParameter("sort_price");
            String appearnace = request.getParameter("invoice_appearance");

            if (searchName != null) {
                invoices = searchName.isEmpty() ? dao.getInvoicesByCusID(cusID)
                        : dao.searchInvoicesByCusIDAndServiceNameOrDoctorName(cusID, searchName);
            } else if (sortPrice != null) {
                invoices = dao.filterInvoicesByCusIDAndSortPrice(cusID, sortPrice);
            } else {
                invoices = dao.filterInvoicesByCusIDAndAppearance(cusID, appearnace);
            }

            if (!invoices.isEmpty()) {
                out.print("<div class=\"container bg-trasparent pb-3 p-2  m-1 pt-0\" id=\"invoices\">\n");
                for (Invoice i : invoices) {
                    out.print("<div class=\"card h-100 shadow-sm row mb-4\" style=\"position: relative;box-shadow: 10px 10px 10px rgba(148, 147, 147, 0.3);\">\n"
                            + "    <div class=\"card-header text-center\" style=\"color: #1d2a4d;font-weight:600;font-size: 20px\">\n"
                            + "      <a href=\"invoice_detail?invoice_id=" + i.getInvoiceID() + "\">\n"
                            + "                            <span style=\"color: #1d2a4d; font-weight: 600; font-size: 20px;\">\n"
                            + "                                Invoice ID: " + i.getInvoiceID()
                            + "                            </span>\n"
                            + "\n"
                            + "                        </a>"
                            + "    </div>\n");

                    for (MedicalReport mr : i.getReports()) {
                        Service ser = mr.getService();
                        out.print("<div class=\"row col-md-11\" style=\"height:70px;border-bottom:2px solid #eceaea;margin: 0px 35px;\">\n"
                                + "    <div class=\"col-md-2 p-2 text-center\">\n"
                                + "        <img src=\"" + ser.getImage() + "\" width=\"85px\" height=\"50px\" alt=\"\" style=\"border-radius:10px;box-shadow:10px 10px 10px #eceaea;\" />\n"
                                + "    </div>\n"
                                + "    <div class=\"col-md-7 p-2 text-center\" style=\"\">\n"
                                + "       <div class=\"item_name\"> <h7 style=\"color: #1d2a4d;font-weight:600\">" + ser.getName() + "</h7></div>\n"
                                + "    </div>\n"
                                + "    <div class=\"col-md-3 p-2 text-center\">\n"
                                + "        <p style=\"color: #1d2a4d;text-align:center;font-weight: 600\">\n"
                                + "            Service " + ser.getId() + ":\n"
                                + "            <span style=\"color: #ff6666; font-size: 20px; font-weight: 600;\">" + ser.getPrice() + " $</span>\n"
                                + "        </p>\n"
                                + "    </div>\n"
                                + "</div>\n");
                    }
                    for (int m = 0; m < i.getMedicineItems().size(); m++) {
                        MedicineItem me = i.getMedicineItems().get(m);
                        out.print("<div class=\"row col-md-11\" style=\"height:70px;border-bottom:2px solid #eceaea;margin: 0px 35px\">\n"
                                + "    <div class=\"col-md-2 p-2 text-center\">\n"
                                + "        <img src=\"" + me.getMedicine().getImage() + "\" width=\"85px\" height=\"50px\" alt=\"\" style=\"border-radius:10px;box-shadow:10px 10px 10px #eceaea;\" />\n"
                                + "    </div>\n"
                                + "    <div class=\"col-md-7 p-2 text-center\" style=\"\">\n"
                                + "       <div class=\"item_name\"> <h7 style=\"color: #1d2a4d;font-weight:600\">" + me.getMedicine().getName() + "</h7></div>\n"
                                + "    </div>\n"
                                + "    <div class=\"col-md-3 p-2 text-center\">\n"
                                + "        <p style=\"color: #1d2a4d;text-align:center;font-weight: 600\">\n"
                                + "            Medicine " + me.getMedicine().getId() + ":\n"
                                + "            <span style=\"color: #ff6666; font-size: 20px; font-weight: 600;\">" + me.getMedicine().getPrice() + " $</span><span style=\"color:#cccccc;\">(x" + me.getMedicineQuantity() + ")</span>\n"
                                + "        </p>\n"
                                + "    </div>\n"
                                + "</div>\n");
                    }

                    out.print("<div class=\"col-md-12 pb-1 card-footer\">\n"
                            + "    <p style=\"color: #1d2a4d;text-align:right;font-weight: 600\">\n"
                            + "        <img src=\"img/cost.png\" width=\"40px\" height=\"40px\" alt=\"alt\"/>TotaL Money:\n"
                            + "        <span style=\"color: #ff6666; font-size: 25px; font-weight: 600;margin-right: 12px;\">" + i.getTotalMoney() + " $</span>\n"
                            + "    </p>\n"
                            + "</div>\n"
                            + " <div class=\"col-md-12 row justify-content-center\">\n"
                            + "                        <div class=\"col-md-3 text-center item_name m-2 p-2\">\n"
                            + "                            <a href=\"profile?s?evaluated_s\">\n"
                            + "                                <img style=\"border: solid #999999 2px; border-radius: 10px;\" src=\"img/evalute.gif\" width=\"40px\" height=\"40px\" alt=\"alt\" />\n"
                            + "                                Evaluate\n"
                            + "                            </a>\n"
                            + "                        </div>\n"
                            + "                        <div class=\"col-md-3 text-center item_name m-2 p-2\">\n"
                            + "                            <a href=\"profile?s?rebook_s\">\n"
                            + "                                <img style=\"border: solid #999999 2px; border-radius: 10px;\" src=\"img/booking.gif\" width=\"40px\" height=\"40px\" alt=\"alt\" />\n"
                            + "                                Rebook\n"
                            + "                            </a>\n"
                            + "                        </div>\n"
                            + "                        <div class=\"col-md-3 text-center item_name m-2 p-2\">\n"
                            + "                            <a href=\"mailto:soncrt1234@gmail.com\">\n"
                            + "                                <img style=\"border: solid #999999 2px; border-radius: 10px;\" src=\"img/mail.gif\" width=\"40px\" height=\"40px\" alt=\"alt\" />\n"
                            + "                                Send email\n"
                            + "                            </a>\n"
                            + "                        </div>\n"
                            + "   </div>"
                            + "</div>\n");
                }
            } else {
                out.print(
                        "            <div style=\"display:block;background:white;margin:auto;box-shadow:10px 10px 10px rgba(148, 147, 147, 0.5);border-radius:10px;padding:200px;height:550px;\">\n"
                        + "                <div>"
                        + "                    <h5 style=\"text-align: center;color:#13c5dd;display:block;\">Not find invoice!</h5>\n"
                        + "                    <img src=\"img/not_find.png\" width=\"150px\" height=\"150px\" style=\"margin:2px 210px\">\n"
                        + "                </div>"
                        + "            </div>\n");

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
