package controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dao.post.PostListDAO;
import dao.service.ServiceListDAO;
import dao.category.CategoryDAO;
import dao.user.DoctorProfileDAO;
import dao.user.LoginDAO;
import dao.user.RegisterDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import model.Constants;
import model.User;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

@WebServlet(name = "LoginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            // TODO output your page here. You may use following sample code.
            LoginDAO lgDAO = new LoginDAO();
            RegisterDAO rgtDAO = new RegisterDAO();
            CategoryDAO dao = new CategoryDAO();
            DoctorProfileDAO daod = new DoctorProfileDAO();
            PostListDAO daop = new PostListDAO();
            ServiceListDAO daoSevices = new ServiceListDAO();
            String service = request.getParameter("service");
            if (service == null) {
                response.sendRedirect("login.jsp");
            } else {
                switch (service) {
                    case "login":
                        String user_email = request.getParameter("user_email");
                        String user_password = request.getParameter("user_password");
                        User user = lgDAO.getUserByEmailAndPassword(user_email, user_password);
                        if (user == null) {
                            request.getRequestDispatcher("login.jsp").forward(request, response);
                        } else {
                            HttpSession session = request.getSession();
                            session.setAttribute("user", user);
                            switch (user.getRole()) {
                                case "1":
                                    response.sendRedirect("HealthCare/home");
                                    break;
                                case "2":
                                    response.sendRedirect("CenterManage/nurse");
                                    break;
                                case "3":
                                    response.sendRedirect("CenterManage/doctor");
                                    break;
                                case "4":
                                    response.sendRedirect("CenterManage/medicine");
                                    break;
                                case "5":
                                    response.sendRedirect("CenterManage/manager");
                                    break;
                            }
                        }
                        break;

                    case "login_via_google":
                        String code = request.getParameter("code");
                        String accessToken = getToken(code);
                        User u = lgDAO.getUserByEmailAndAccountType(getUserInfo(accessToken).getEmail(), "google");
                        u.setAvatarUrl(getUserInfo(accessToken).getAvatarUrl());
                        HttpSession session = request.getSession();
                        session.setAttribute("user", u);
                        switch (u.getRole()) {
                                case "1":
                                    response.sendRedirect("HealthCare/home");
                                    break;
                                case "2":
                                    response.sendRedirect("CenterManage/nurse");
                                    break;
                                case "3":
                                    response.sendRedirect("CenterManage/doctor");
                                    break;
                                case "4":
                                    response.sendRedirect("CenterManage/medicine");
                                    break;
                                case "5":
                                    response.sendRedirect("CenterManage/manager");
                                    break;
                            }
                        
                        break;

                }
            }
        }
    }

    public static String getToken(String code) throws ClientProtocolException, IOException {
        // call api to get token
        String response = Request.Post(Constants.GOOGLE_LINK_GET_TOKEN)
                .bodyForm(Form.form().add("client_id", Constants.GOOGLE_CLIENT_ID)
                        .add("client_secret", Constants.GOOGLE_CLIENT_SECRET)
                        .add("redirect_uri", Constants.GOOGLE_REDIRECT_URI).add("code", code)
                        .add("grant_type", Constants.GOOGLE_GRANT_TYPE).build())
                .execute().returnContent().asString();

        JsonObject jobj = new Gson().fromJson(response, JsonObject.class
        );
        String accessToken = jobj.get("access_token").toString().replaceAll("\"", "");
        return accessToken;
    }

    public static User getUserInfo(final String accessToken) throws ClientProtocolException, IOException {
        String link = Constants.GOOGLE_LINK_GET_USER_INFO + accessToken;
        String response = Request.Get(link).execute().returnContent().asString();
        User googlePojo = new Gson().fromJson(response, User.class
        );

        return googlePojo;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        // Add code for handling POST request if needed
    }

    @Override
    public String getServletInfo() {
        return "Servlet for handling user_id login";
    }
}
