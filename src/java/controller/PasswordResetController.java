/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.user.PasswordChangeDAO;
import dao.user.PasswordResetDAO;
import jakarta.servlet.RequestDispatcher;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "PasswordResetController", urlPatterns = {"/password_reset"})
public class PasswordResetController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if (request.getParameter("step") == null) {
            response.sendRedirect("password_reset.jsp");
            return;
        }
        if (request.getParameter("step").equals("2")) {
            HttpSession mySession = request.getSession();
            String sessionOtp = mySession.getAttribute("otp").toString();
            String otp = request.getParameter("otp");
            if (!sessionOtp.equals(otp)) {
                response.getWriter().write("fail");
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String step = request.getParameter("step");
        User u = new User();
        HttpSession mySession = request.getSession();
        switch (step) {
            case "1":
                String iden = request.getParameter("identifier");
                PasswordResetDAO dao = new PasswordResetDAO();
                u = dao.FindUserByIdentifier(iden);
                if (u != null) {

                    RequestDispatcher dispatcher = null;
                    int otpvalue = 0;
                    Random rand = new Random();
                    otpvalue = rand.nextInt(1255650);

                    String to = u.getEmail();
                    Properties props = new Properties();
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.socketFactory.port", "465");
                    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.port", "465");
                    Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("minhpthe171392@fpt.edu.vn", "tmfsbdnechouawsp");// Put your email
                            // id and
                            // password here
                        }
                    });
                    try {
                        MimeMessage message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(to));// change accordingly
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                        message.setSubject("Hello");
                        message.setText("your OTP is: " + otpvalue);
                        // send message
                        Transport.send(message);
                        System.out.println("message sent successfully");
                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }
                    mySession.setAttribute("otp", otpvalue);
                    mySession.setAttribute("email", to);
                    mySession.setAttribute("userID", u.getId());
                    System.out.println(mySession.getAttribute("otp"));
                }
                request.setAttribute("user", u);
                request.setAttribute("step", "2");
                request.getRequestDispatcher("password_reset.jsp").forward(request, response);
                break;
            case "2":
                String sessionOtp = mySession.getAttribute("otp").toString();
                String otp = request.getParameter("otp");
                if (sessionOtp.equals(otp)) {
                    mySession.removeAttribute("otp");
                    request.setAttribute("step", "3");
                    request.getRequestDispatcher("password_reset.jsp").forward(request, response);
                }
                String message = "OTP is not correct. Please enter again.";
                request.setAttribute("message", message);
                request.setAttribute("step", "2");
                request.getRequestDispatcher("password_reset.jsp").forward(request, response);
                break;
            case "3":
                u = new User(mySession.getAttribute("userID").toString());
                String newPass = request.getParameter("newPass");
                PasswordChangeDAO DAO = new PasswordChangeDAO();
                DAO.updatePassword(u, newPass);
                request.getRequestDispatcher("login.jsp").forward(request, response);
                break;
        }
    }

    public void removeOTPSession(HttpServletRequest request, HttpServletResponse response) {
        HttpSession mySession = request.getSession();
        mySession.removeAttribute("otp");
    }

}
