/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.user.PasswordChangeDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.User;

/**
 *
 * @author LT
 */
@WebServlet(name = "PasswordChangeController", urlPatterns = {"/password_change"})
public class PasswordChangeController extends HttpServlet {

    private static final String PASSWORD_PATTERN
            = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])"
            + "(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

    private static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

    public static boolean isValid(final String password) {
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("/SWP391/login");
        } else {
            response.sendRedirect("password_change.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean check = false;
        String inputMess = "";
        String newMess = "";
        String reMess = "";
        String mess = "";
        String inputPass = request.getParameter("inputPass");
        String newPass = request.getParameter("newPass");
        String rePass = request.getParameter("rePass");
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        PasswordChangeDAO DAO = new PasswordChangeDAO();
        System.out.println(u);
        if (!DAO.PasswordVerification(u, inputPass)) {
            inputMess += "Current password does not match.";
        }
        if (!isValid(newPass)) {
            newMess += "Password is not valid.";
        }
        if (!rePass.equals(newPass)) {
            reMess += "Re-password must be similar to new password.";
        }
        if (inputMess.isEmpty() && newMess.isEmpty() && reMess.isEmpty()) {
            mess += "Password changed successfully!";
            check = true;
        }
        if (check) {
            DAO.updatePassword(u, newPass);
            response.sendRedirect("/SWP391/profile_detail");
        } else {
            request.setAttribute("inputMess", inputMess);
            request.setAttribute("newMess", newMess);
            request.setAttribute("reMess", reMess);
            request.setAttribute("mess", mess);
            request.getRequestDispatcher("password_change.jsp").forward(request, response);
        }
    }

}
