/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Son Nguyen
 */
public class DoctorSchedule {
    private String doctorScheduleId;
    private User user;
    private Slot slot;
    private String numberReservation;
    private  int dayOfWeek;

    public DoctorSchedule(String doctorScheduleId, User user, Slot slot, String numberReservation) {
        this.doctorScheduleId = doctorScheduleId;
        this.user = user;
        this.slot = slot;
        this.numberReservation = numberReservation;
    }

    public DoctorSchedule(String doctorScheduleId, User user, Slot slot, String numberReservation, int dayOfWeek) {
        this.doctorScheduleId = doctorScheduleId;
        this.user = user;
        this.slot = slot;
        this.numberReservation = numberReservation;
        this.dayOfWeek = dayOfWeek;
    }
    
    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
   
    public String getDoctorScheduleId() {
        return doctorScheduleId;
    }

    public void setDoctorScheduleId(String doctorScheduleId) {
        this.doctorScheduleId = doctorScheduleId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String getNumberReservation() {
        return numberReservation;
    }

    public void setNumberReservation(String numberReservation) {
        this.numberReservation = numberReservation;
    }
    
    
    
}
