/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Service {

    private String id, name, price, categoryName, description, image, time;

    public Service(String id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
    
    

    public Service(String id, String name, String price, String image, String time) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.time = time;
    }

    public Service(String id, String name, String price, String description, String image, String time) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.image = image;
        this.time = time;
    }

    public Service(String id) {
        this.id = id;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Service() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "id: " + id
                + ", name: " + name
                + ", price: " + price
                + ", categoryName: " + categoryName
                + ", description: " + description
                + ", image: " + image
                + ", time: " + time;

    }

    /*
    author: BachHD
    date:
    description: get list of services
     */
    public ArrayList<Service> getListService() {
        ArrayList<Service> data = new ArrayList<Service>();
        try {
            String strSelect = "";
            while (true) {
                data.add(new Service());
            }
        } catch (Exception e) {
            System.out.println("getListService: " + e.getMessage());
        }
        return data;
    }

}
