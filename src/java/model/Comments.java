/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author LT
 */

public class Comments {
    private String comment_id;
    private String commentText;
 

    public Comments() {
    }

    public Comments(String comment_id,String comment_text) {
        this.comment_id = comment_id;
        this.commentText = comment_text;
    }

    

    public String getComment_text() {
        return commentText;
    }

    public void setComment_text(String comment_text) {
        this.commentText = comment_text;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    @Override
    public String toString() {
        return "Comments{" + "comment_id=" + comment_id + ", commentText=" + commentText + '}';
    }


    
    
    
}
