/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Son Nguyen
 */
public class MedicineItem {

    private String invoiceID;
    private Medicine medicine;
    private int medicineQuantity;

    public MedicineItem(String invoiceID, Medicine medicine, int medicineQuantity) {
        this.invoiceID = invoiceID;
        this.medicine = medicine;
        this.medicineQuantity = medicineQuantity;
    }

    public String getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(String invoiceID) {
        this.invoiceID = invoiceID;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public int getMedicineQuantity() {
        return medicineQuantity;
    }

    public void setMedicineQuantity(int medicineQuantity) {
        this.medicineQuantity = medicineQuantity;
    }

}
