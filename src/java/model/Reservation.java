/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class Reservation {

    private String reservationID;
    
    private String cusName;
    private String cusDOB;
    private String cusPhone;
    private String cusEmail;
    
    private List<Service> services;
    
    private String docID;
    private String docName;
    private String serviceStartTime;
    private String serviceEndTime;
    private Date dateAndTime;
    private String status;

    public Reservation() {
    }

    
    
    
    public Reservation(String reservationID, String cusName, String cusDOB, String cusPhone, String cusEmail, List<Service> services, String docID, String docName, String serviceStartTime, String serviceEndTime, Date dateAndTime, String status) {
        this.reservationID = reservationID;
        this.cusName = cusName;
        this.cusDOB = cusDOB;
        this.cusPhone = cusPhone;
        this.cusEmail = cusEmail;
        this.services = services;
        this.docID = docID;
        this.docName = docName;
        this.serviceStartTime = serviceStartTime;
        this.serviceEndTime = serviceEndTime;
        this.dateAndTime = dateAndTime;
        this.status = status;
    }

    

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

   

   

    public String getReservationID() {
        return reservationID;
    }

    public void setReservationID(String reservationID) {
        this.reservationID = reservationID;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusDOB() {
        return cusDOB;
    }

    public void setCusDOB(String cusDOB) {
        this.cusDOB = cusDOB;
    }

    public String getCusPhone() {
        return cusPhone;
    }

    public void setCusPhone(String cusPhone) {
        this.cusPhone = cusPhone;
    }

    public String getCusEmail() {
        return cusEmail;
    }

    public void setCusEmail(String cusEmail) {
        this.cusEmail = cusEmail;
    }

   

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getServiceStartTime() {
        return serviceStartTime;
    }

    public void setServiceStartTime(String serviceStartTime) {
        this.serviceStartTime = serviceStartTime;
    }

    public String getServiceEndTime() {
        return serviceEndTime;
    }

    public void setServiceEndTime(String serviceEndTime) {
        this.serviceEndTime = serviceEndTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

 
    @Override
    public String toString() {
        return "Reservation Information:\n"
                + "Reservation id: " + reservationID + "\n"
                + "Customer Name: " + cusName + "\n"
                + "Customer Date of Birth: " + cusDOB + "\n"
                + "Customer Phone: " + cusPhone + "\n"
                + "Customer Email: " + cusEmail + "\n"
                + "Service List: " + services + "\n"
                + "Doctor ID: " + docID + "\n"
                + "Doctor Name: " + docName + "\n"
                + "Service Start Time: " + serviceStartTime + "\n"
                + "Service End Time: " + serviceEndTime + "\n"
                + "Date And Time: " + dateAndTime + "\n"
                + "Status: " + status + "\n\n";
    }

}
