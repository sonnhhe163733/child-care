/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class Post {
    private int post_id;
    private int post_author_id;
    private String post_description,post_created_date,post_content,post_souce_url;

    public Post() {
    }

    public Post(int post_id, int post_author_id, String post_description, String post_created_date, String post_content, String post_souce_url) {
        this.post_id = post_id;
        this.post_author_id = post_author_id;
        this.post_description = post_description;
        this.post_created_date = post_created_date;
        this.post_content = post_content;
        this.post_souce_url = post_souce_url;
    }

    public Post(int aInt, int aInt0, String string) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
     
    

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getPost_author_id() {
        return post_author_id;
    }

    public void setPost_author_id(int post_author_id) {
        this.post_author_id = post_author_id;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getPost_created_date() {
        return post_created_date;
    }

    public void setPost_created_date(String post_created_date) {
        this.post_created_date = post_created_date;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_souce_url() {
        return post_souce_url;
    }

    public void setPost_souce_url(String post_souce_url) {
        this.post_souce_url = post_souce_url;
    }

    @Override
    public String toString() {
        return "Post{" + "post_id=" + post_id + ", post_author_id=" + post_author_id + ", post_description=" + post_description + ", post_created_date=" + post_created_date + ", post_content=" + post_content + ", post_souce_url=" + post_souce_url + '}';
    }
    
    
}
