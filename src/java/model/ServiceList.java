/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Son Nguyen
 */
public class ServiceList {

    private List<Service> services;

    public ServiceList() {
    }
    

    public boolean isContainServiceList(String name, List<Service> services) {
        for (Service s : services) {
            if (name.contains(s.toString())) {
                return true;
            }
        }
        return false;
    }
}
